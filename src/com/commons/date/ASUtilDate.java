package com.commons.date;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ASUtilDate extends Date {
    public static final SimpleDateFormat formatDateFile = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat formatDateTimeFile = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public ASUtilDate() {
    }

    public ASUtilDate(Date date) {
        super(date.getTime());
    }

    public ASUtilDate(String dateAsString) throws Exception {
        if (dateAsString != null && !dateAsString.equals("")) {
            if (dateAsString.length() == 10) {
                this.setTime((new SimpleDateFormat("yyyy-MM-dd")).parse(dateAsString).getTime());
            } else if (dateAsString.length() == 16) {
                this.setTime((new SimpleDateFormat("yyyy-MM-dd HH:mm")).parse(dateAsString).getTime());
            } else if (dateAsString.length() == 19) {
                this.setTime((new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).parse(dateAsString).getTime());
            } else if (dateAsString.length() == 20) {
                this.setTime((new SimpleDateFormat("yyyy-MM-dd HH:mm:sss")).parse(dateAsString).getTime());
            } else {
                if (dateAsString.length() != "yyyy-MM-dd'T'HH:mm:ss'Z'".length()) {
                    throw new Exception("Unparsable Date: " + dateAsString);
                }

                this.setTime((new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")).parse(dateAsString).getTime());
            }

        } else {
            throw new Exception("Not valid date");
        }
    }

    public String toFileDate() {
        return (new SimpleDateFormat("yyyy-MM-dd")).format(this.getTime());
    }

    public String toFileDateTime() {
        return (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(this.getTime());
    }
}
