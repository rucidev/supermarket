package com.commons.date;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ASDateUtils {
    public static final SimpleDateFormat formatDateTime = new SimpleDateFormat("dd/MM/yyyy");
    public static final SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy HH:MM");

    public ASDateUtils() {
    }

    public static int getHours(ASUtilDate date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(11);
    }

    public static int getMinutes(ASUtilDate date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(12);
    }

    public static int getSeconds(ASUtilDate date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(13);
    }

    public static ASUtilDate getFirstDateOfYear() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(6, 1);
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.set(14, 0);
        return new ASUtilDate(calendar.getTime());
    }

    public static ASUtilDate getDateAfterToday(int afterNoDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.set(14, 0);
        calendar.add(5, afterNoDate);
        return new ASUtilDate(calendar.getTime());
    }

    public static ASUtilDate getDateAfterAddingMinutes(ASUtilDate newDate, int afterNoOfMinutes) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(newDate);
        calendar.add(12, afterNoOfMinutes);
        return new ASUtilDate(calendar.getTime());
    }

    public static ASUtilDate getDateAfterAddingHours(ASUtilDate newDate, int afterNoOfHours) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(newDate);
        calendar.add(11, afterNoOfHours);
        return new ASUtilDate(calendar.getTime());
    }

    public static ASUtilDate getDateAfterAddingHoursMinutes(ASUtilDate newDate, int afterNoOfHours, int afterNoOfMinutes) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(newDate);
        calendar.add(11, afterNoOfHours);
        calendar.add(12, afterNoOfMinutes);
        return new ASUtilDate(calendar.getTime());
    }

    public static ASUtilDate getDateAfterAddingMillis(ASUtilDate newDate, long afterNoMillis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(newDate.getTime() + afterNoMillis);
        return new ASUtilDate(calendar.getTime());
    }

    public static ASUtilDate getDateAfterSettingMinutes(ASUtilDate newDate, int afterNoOfMinutes) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(newDate);
        calendar.set(12, afterNoOfMinutes);
        return new ASUtilDate(calendar.getTime());
    }

    public static ASUtilDate getDateAfterSettingHours(ASUtilDate newDate, int afterNoOfHours) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(newDate);
        calendar.set(11, afterNoOfHours);
        return new ASUtilDate(calendar.getTime());
    }

    public static ASUtilDate getDateAfterSettingHoursMinutes(ASUtilDate newDate, int afterNoOfHours, int afterNoOfMinutes) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(newDate);
        calendar.set(11, afterNoOfHours);
        calendar.set(12, afterNoOfMinutes);
        return new ASUtilDate(calendar.getTime());
    }

    public static ASUtilDate getCurrentDateWithTimeOfTheOther(ASUtilDate timeDate) {
        ASUtilDate newDate = new ASUtilDate();
        return getDateWithTimeOfTheOther(newDate, timeDate);
    }

    public static ASUtilDate getDateWithTimeOfTheOther(ASUtilDate newDate, ASUtilDate timeDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(newDate);
        Calendar calendar1 = (Calendar)calendar.clone();
        calendar1.setTime(timeDate);
        calendar.set(11, calendar1.get(11));
        calendar.set(12, calendar1.get(12));
        calendar.set(13, calendar1.get(13));
        calendar.set(14, calendar1.get(14));
        return new ASUtilDate(calendar.getTime());
    }

    public static ASUtilDate getDateWithTime(int hours, int minutes) {
        return getDateWithTime(hours, minutes, 0);
    }

    public static ASUtilDate getDateWithTime(int hours, int minutes, int seconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(11, hours);
        calendar.set(12, minutes);
        calendar.set(13, seconds);
        return new ASUtilDate(calendar.getTime());
    }

    public static ASUtilDate getDateTimeFromString(String dateString) throws Exception {
        return new ASUtilDate(formatDateTime.parse(dateString));
    }

    public static ASUtilDate getFirstDateOfMonth(Calendar calendar) {
        calendar.set(5, 1);
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.set(14, 0);
        System.out.println("getFirstDateOfMonth=" + formatDate.format(calendar.getTime()));
        return new ASUtilDate(calendar.getTime());
    }

    public static ASUtilDate getFirstDateOfMonth() {
        Calendar calendar = Calendar.getInstance();
        return getFirstDateOfMonth(calendar);
    }

    public static ASUtilDate getLastDateOfMonth() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(5, calendar.getActualMaximum(5));
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.set(14, 0);
        System.out.println("getLastDateOfMonth=" + formatDate.format(calendar.getTime()));
        return new ASUtilDate(calendar.getTime());
    }

    public static ASUtilDate getFirstDateOfNextMonth(Calendar calendar) {
        calendar.set(5, 1);
        calendar.add(2, 1);
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.set(14, 0);
        System.out.println("getFirstDateOfNextMonth=" + formatDate.format(calendar.getTime()));
        return new ASUtilDate(calendar.getTime());
    }

    public static ASUtilDate getFirstDateOfNextMonth() {
        Calendar calendar = Calendar.getInstance();
        return getFirstDateOfNextMonth(calendar);
    }

    public static String getDateDiffFromDateToHour(ASUtilDate date1, ASUtilDate date2, String day, String hour, String min) {
        String str = "";
        long daysDiff = (long)getDaysDiff(date1, date2);
        long hoursDiff = getHoursDiffModule(date1, date2);
        long minutesDiff = getMinutesDiffModule(date1, date2);
        if (daysDiff == 0L) {
            if (hoursDiff == 0L) {
                str = minutesDiff + " " + min;
            } else {
                str = hoursDiff + " " + hour + " " + getDateToHourValueIfNotZero(minutesDiff, min);
            }
        } else {
            str = daysDiff + " " + day + ", " + getDateToHourValueIfNotZero(hoursDiff, hour + ", ") + getDateToHourValueIfNotZero(minutesDiff, min);
        }

        return str;
    }

    private static String getDateToHourValueIfNotZero(long value, String label) {
        return value != 0L ? value + " " + label : "";
    }

    public static double getHoursAndMinutesDiffModule(ASUtilDate start, ASUtilDate end) {
        return (double)getHoursDiffModule(start, end) + (double)getMinutesDiffModule(start, end) / 60.0D;
    }

    public static long getDaysDiffModule(ASUtilDate date1, ASUtilDate date2) {
        return (long)getDaysDiff(date1, date2);
    }

    public static double getDaysDiff(ASUtilDate date1, ASUtilDate date2) {
        return getHoursDiff(date1, date2) / 24.0D;
    }

    public static long getHoursDiffModule(ASUtilDate date1, ASUtilDate date2) {
        return (long)getHoursDiff(date1, date2) % 24L;
    }

    public static double getHoursDiff(ASUtilDate date1, ASUtilDate date2) {
        return getMinutesDiff(date1, date2) / 60.0D;
    }

    public static long getMinutesDiffModule(ASUtilDate date1, ASUtilDate date2) {
        return (long)getMinutesDiff(date1, date2) % 60L;
    }

    public static double getMinutesDiff(ASUtilDate date1, ASUtilDate date2) {
        return getSecondsDiff(date1, date2) / 60.0D;
    }

    public static long getSecondsDiffModule(ASUtilDate date1, ASUtilDate date2) {
        return (long)getSecondsDiff(date1, date2) % 60L;
    }

    public static double getSecondsDiff(ASUtilDate date1, ASUtilDate date2) {
        return (double)(getMilliSecondsDiff(date1, date2) / 1000L);
    }

    public static long getMilliSecondsDiff(ASUtilDate date1, ASUtilDate date2) {
        return date2.getTime() - date1.getTime();
    }

    public static String getDateFormatted(ASUtilDate newDate) {
        return formatDate.format(newDate.getTime());
    }

    public static String getDateTimeFormatted(ASUtilDate newDate) {
        return formatDateTime.format(newDate.getTime());
    }
}
