package com.commons.entity;

import com.commons.date.ASUtilDate;

public class Entity {
    public static final int MINUS_ONE = -1;
    private int id = -1;
    private String description = "";
    private String description1 = "";
    private ASUtilDate cDate;
    private ASUtilDate mDate;
    private boolean active = true;

    public Entity() {
    }

    public Entity(int id) {
        this.setId(id);
    }

    public Entity(String description) {
        this.setDescription(description);
    }

    public Entity(int id, String description) {
        this.setId(id);
        this.setDescription(description);
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ASUtilDate getcDate() {
        if (this.cDate == null) {
            this.cDate = new ASUtilDate();
        }

        return this.cDate;
    }

    public void setcDate(ASUtilDate cDate) {
        this.cDate = cDate;
    }

    public ASUtilDate getmDate() {
        if (this.mDate == null) {
            this.mDate = new ASUtilDate();
        }

        return this.mDate;
    }

    public void setmDate(ASUtilDate mDate) {
        this.mDate = mDate;
    }

    public boolean isActive() {
        return this.active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getDescription1() {
        return this.description1;
    }

    public void setDescription1(String description1) {
        this.description1 = description1;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (!(o instanceof Entity)) {
            return false;
        } else {
            Entity entity = (Entity)o;
            if (this.getId() != entity.getId()) {
                return false;
            } else if (this.isActive() != entity.isActive()) {
                return false;
            } else {
                label57: {
                    if (this.getDescription() != null) {
                        if (this.getDescription().equals(entity.getDescription())) {
                            break label57;
                        }
                    } else if (entity.getDescription() == null) {
                        break label57;
                    }

                    return false;
                }

                if (this.getDescription1() != null) {
                    if (!this.getDescription1().equals(entity.getDescription1())) {
                        return false;
                    }
                } else if (entity.getDescription1() != null) {
                    return false;
                }

                if (this.getcDate() != null) {
                    if (this.getcDate().equals(entity.getcDate())) {
                        return this.getmDate() != null ? this.getmDate().equals(entity.getmDate()) : entity.getmDate() == null;
                    }
                } else if (entity.getcDate() == null) {
                    return this.getmDate() != null ? this.getmDate().equals(entity.getmDate()) : entity.getmDate() == null;
                }

                return false;
            }
        }
    }

    public String toString() {
        return "Entity{id=" + this.id + ", description='" + this.description + '\'' + ", description1='" + this.description1 + '\'' + ", cDate=" + this.cDate + ", mDate=" + this.mDate + ", active=" + this.active + '}';
    }
}
