package com.dblayer;

import com.commons.date.ASUtilDate;
import com.model.product.Product;
import com.model.product.SoldProduct;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class SoldProductDBLayer {
    private final String fileName = "other\\db\\soldproduct";

    public void saveSoldProduct(SoldProduct soldProduct){

        try{
            FileWriter fw = new FileWriter(fileName, true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter writer = new PrintWriter(bw);

                writer.write("\n" + soldProduct.getId() + " ");
                writer.write(soldProduct.getName() + " ");
                writer.write(soldProduct.getSellingPrice() + " ");
                writer.write(soldProduct.getBillID() + " ");
                writer.write(soldProduct.getSoldQuantity() + " ");
                writer.write(soldProduct.getSoldDate().toFileDate() + " ");

            writer.close();
        }
        catch (Exception ex){
            System.out.println(
                    "Unable to open file '" +
                            fileName + "'");
        }
    }

    public ArrayList<SoldProduct> getSoldProductsByBillID() {
        ArrayList<SoldProduct> products = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(new File(fileName));

            String line = scanner.nextLine();

            while (scanner.hasNext()) {
                int id = scanner.nextInt();
                String name = scanner.next();
                double price = scanner.nextDouble();
                int billdId = scanner.nextInt();
                int quantity = scanner.nextInt();
                String soldDate = scanner.next();

                SoldProduct product = new SoldProduct();
                product.setId(id);
                product.setName(name);
                product.setSellingPrice(price);
                product.setBillID(billdId);
                product.setSoldQuantity(quantity);
                product.setSoldDate(new ASUtilDate(soldDate));
                products.add(product);
            }
        }
        catch (Exception ex) {
            System.out.println(
                    "Unable to open file '" +
                            fileName + "'");
        }
        return products;
    }

    public ArrayList<SoldProduct> getSoldProducts() {
        ArrayList<SoldProduct> products = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(new File(fileName));
            String line = scanner.nextLine();
            while (scanner.hasNext()) {
                SoldProduct product = new SoldProduct();
                setSoldProductValues(product, Integer.parseInt(scanner.next()),
                        scanner.next(),  Double.parseDouble(scanner.next()),
                        Integer.parseInt(scanner.next()),
                        Integer.parseInt(scanner.next()), scanner.next());
                products.add(product);
            }
        }
        catch (Exception ex) {
            System.out.println(
                    "Unable to open file '" +
                            fileName + "'");
            ex.printStackTrace();
        }
        return products;
    }

    public ArrayList<SoldProduct> getSoldProductsByDate(String date1, String date2) {
        ArrayList<SoldProduct> soldProducts = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(new File(fileName));
            String line = scanner.nextLine();
            while (scanner.hasNext()) {
                SoldProduct soldProduct = new SoldProduct();
                setSoldProductValues(soldProduct, Integer.parseInt(scanner.next()),
                        scanner.next(),  Double.parseDouble(scanner.next()),
                        Integer.parseInt(scanner.next()),
                        Integer.parseInt(scanner.next()), scanner.next());
                String billDate = soldProduct.getSoldDate().toFileDate();
                int compareRes = billDate.compareTo(date1);
                System.out.println(compareRes);
                int secondCompareRes = billDate.compareTo(date2);
                if(compareRes != -1 && secondCompareRes != 1){
                    soldProducts.add(soldProduct);
                }
            }
        }
        catch (Exception ex) {
            System.out.println(
                    "Unable to open file '" +
                            fileName + "'");
            ex.printStackTrace();
        }
        return soldProducts;
    }

    private void setSoldProductValues(SoldProduct product, int id, String data, double price, int billId, int quantity, String soldDate) {
        try {
            product.setId(id);
            product.setName(data);
            product.setSellingPrice(price);
            product.setQuantity(quantity);
            product.setSupplierID(billId);
            product.setSoldDate(new ASUtilDate(soldDate));
        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }




//    public SoldProduct getProductById(int filter) throws DBException {
//        SoldProduct product = new SoldProduct();
//        boolean productFound = false;
//        try {
//            Scanner scanner = new Scanner(new File(fileName));
//
//            String line = scanner.nextLine();
//            int data;
//
//            while (scanner.hasNext()) {
//                data = scanner.nextInt();
//                if(data == filter){
//                    product.setId(data);
//                    product.setName(scanner.next());
//                    product.setSupplierID(scanner.nextInt());
//                    productFound = true;
//                    break;
//                }
//            }
//        } catch (Exception ex) {
//            System.out.println(
//                    "Unable to open file '" +
//                            fileName + "'");
//        }
//        if(!productFound){
//            throw new DBException("Product not in stock!");
//        }
//        return product;
//    }
//
//    public SoldProduct getProductByName(String filter) throws DBException {
//        SoldProduct product = new SoldProduct();
//        boolean productFound = false;
//        try {
//            Scanner scanner = new Scanner(new File(fileName));
//
//            String line = scanner.nextLine();
//            String data;
//            int id, supplierId;
//
//            while (scanner.hasNext()) {
//                id = scanner.nextInt();
//                data = scanner.next();
//                supplierId = scanner.nextInt();
//                System.out.println(data);
//                if(data.equals(filter)){
//                    product.setId(id);
//                    product.setName(data);
//                    product.setSupplierID(supplierId);
//                    productFound = true;
//                    break;
//                }
//            }
//        } catch (Exception ex) {
//            System.out.println(
//                    "Unable to open file '" +
//                            fileName + "'");
//        }
//        if(!productFound){
//            throw new DBException("Product not in stock!");
//        }
//        return product;
//    }
}
