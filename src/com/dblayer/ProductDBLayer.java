package com.dblayer;

import com.commons.date.ASUtilDate;
import com.model.exception.DBException;
import com.model.product.Product;
import com.model.product.SoldProduct;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class ProductDBLayer implements ProductDBCharacteristics{
    private final String fileName = "other\\db\\product";

    public ArrayList<Product> getProductsByDate(String date1, String date2){
        ArrayList<Product> products = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(new File(fileName));
            String line = scanner.nextLine();
            while (scanner.hasNext()) {
                Product product = new Product();
                setProductValues(product, Integer.parseInt(scanner.next()),
                        scanner.next(),  Double.parseDouble(scanner.next()), Integer.parseInt(scanner.next()),
                        Integer.parseInt(scanner.next()), Double.parseDouble(scanner.next()), new ASUtilDate(scanner.next()));
                String billDate = product.getBoughtDate().toFileDate();
                int compareRes = billDate.compareTo(date1);
                int secondCompareRes = billDate.compareTo(date2);
                if(compareRes != -1 && secondCompareRes != 1){
                    products.add(product);
                }
            }
        }
        catch (Exception ex) {
            System.out.println(
                    "Unable to open file '" +
                            fileName + "'");
            ex.printStackTrace();
        }
        return products;
    }

    public void updateProduct(Product product){
        try{
            ArrayList<Product> products = getProducts();
            for (Product product1 : products) {
                if(product.getId() == product1.getId()){
                    product1.setQuantity(product.getQuantity());
                }
            }

            FileWriter fw = new FileWriter(fileName, false);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter writer = new PrintWriter(bw);
            writer.write("ID NAME SELLINGPRICE QUANTITY SUPPLIERID BOUGHTPRICE BOUGHTDATE");
            for(Product product1: products){
                writer.write("\n" + product1.getId() + " ");
                writer.write(product1.getName() + " ");
                writer.write(product1.getSellingPrice() + " ");
                writer.write(product1.getQuantity() + " ");
                writer.write(product1.getSupplierID() + " ");
                writer.write(product1.getBoughtPrice() + " ");
                writer.write(product1.getBoughtDate().toFileDate());
            }
            writer.close();
        }
        catch (Exception ex){
            System.out.println(
                    "Unable to open file '" +
                            fileName + "'");
        }
    }

    public Product getProductByName(String filter) throws DBException {
        Product product = new Product();
        boolean productFound = false;
        try {
            Scanner scanner = new Scanner(new File(fileName));

            String line = scanner.nextLine();
            String data, boughtDate;
            int id, supplierId;
            double price;
            double boughtPrice;
            int quantity;

            while (scanner.hasNext()) {
                id = scanner.nextInt();
                data = scanner.next();
                price = scanner.nextDouble();
                quantity = scanner.nextInt();
                supplierId = scanner.nextInt();
                boughtPrice = scanner.nextDouble();
                boughtDate = scanner.next();
                if(data.equals(filter)){
                    setProductValues(product, id, data, price, quantity, supplierId, boughtPrice, new ASUtilDate(boughtDate));
                    productFound = true;
                    break;
                }
            }
        } catch (Exception ex) {
            System.out.println(
                    "Unable to open file '" +
                            fileName + "'");
        }
        if(!productFound){
            throw new DBException("Product not in stock!");
        }
        return product;
    }

    public ArrayList<Product> getProducts() {
        ArrayList<Product> products = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(new File(fileName));

            String line = scanner.nextLine();

            while (scanner.hasNext()) {
                Product product = new Product();
                setProductValues(product, Integer.parseInt(scanner.next()),
                        scanner.next(),  Double.parseDouble(scanner.next()), Integer.parseInt(scanner.next()),
                        Integer.parseInt(scanner.next()), Double.parseDouble(scanner.next()), new ASUtilDate(scanner.next()));
                products.add(product);
            }
        }
        catch (Exception ex) {
            System.out.println(
                    "Unable to open file '" +
                            fileName + "'");
            ex.printStackTrace();
        }
        return products;
    }

    public ArrayList<Product> getProductsBySupplierID(int filter) throws DBException {
        ArrayList<Product> products = new ArrayList<>();
        boolean productFound = false;
        try {
            Scanner scanner = new Scanner(new File(fileName));

            String line = scanner.nextLine();
            String data;
            int id, supplierId;
            double price;
            double boughtPrice;
            int quantity;
            String boughtDate;

            while (scanner.hasNext()) {
                id = scanner.nextInt();
                data = scanner.next();
                price = scanner.nextDouble();
                quantity = scanner.nextInt();
                supplierId = scanner.nextInt();
                boughtPrice = scanner.nextDouble();
                boughtDate = scanner.next();

                Product product = new Product();
                if(supplierId == filter){
                    setProductValues(product, id, data, price, quantity, supplierId, boughtPrice, new ASUtilDate(boughtDate));
                    products.add(product);
                    productFound = true;
                }
            }
        } catch (Exception ex) {
            System.out.println(
                    "Unable to open file '" +
                            fileName + "'");
        }
        if(!productFound){
            throw new DBException("Product not in stock!");
        }
        return products;
    }



    public Product getProductById(int filter) throws DBException {
        Product product = new Product();
        boolean productFound = false;
        try {
            Scanner scanner = new Scanner(new File(fileName));

            String line = scanner.nextLine();
            String data;
            int id, supplierId;
            double price;
            double boughtPrice;
            int quantity;
            String boughtDate;

            while (scanner.hasNext()) {
                id = scanner.nextInt();
                data = scanner.next();
                price = scanner.nextDouble();
                quantity = scanner.nextInt();
                supplierId = scanner.nextInt();
                boughtPrice = scanner.nextDouble();
                boughtDate = scanner.next();
                if(id == filter){
                    setProductValues(product, id, data, price, quantity, supplierId, boughtPrice, new ASUtilDate(boughtDate));
                    productFound = true;
                    break;
                }
            }
        } catch (Exception ex) {
            System.out.println(
                    "Unable to open file '" +
                            fileName + "'");
        }
        if(!productFound){
            throw new DBException("Product not in stock!");
        }
        return product;
    }

    private void setProductValues(Product product, int id, String data,  double price, int quantity, int supplierId, double boughtPrice, ASUtilDate boughtDate) {
        product.setId(id);
        product.setName(data);
        product.setSellingPrice(price);
        product.setQuantity(quantity);
        product.setSupplierID(supplierId);
        product.setBoughtPrice(boughtPrice);
        product.setBoughtDate(boughtDate);
    }
}
