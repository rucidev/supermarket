package com.dblayer;

import com.commons.date.ASUtilDate;
import com.gui.Main;
import com.model.bill.Bill;
import com.model.employee.User;
import com.model.product.Product;
import com.model.product.SoldProduct;
import com.sun.javafx.iio.bmp.BMPImageLoaderFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class BillDBLayer {
    private static final String BILL_ID_FILE_NAME = "other\\db\\billid";
    private static final String BILL_FILE_NAME = "other\\db\\bill";
    private static int id;

    public static void saveBill(Bill bill){

        try{
            FileWriter fw = new FileWriter(BILL_FILE_NAME, true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter writer = new PrintWriter(bw);

            writer.write("\n" + bill.getId() + " ");
            writer.write(bill.getCashier().getName() + " ");
            writer.write(bill.getTotalPrice() + " ");
            writer.write(bill.getNrItems() + " ");
            writer.write(bill.getcDate().toFileDate() + " ");

            writer.close();
        }
        catch (Exception ex){
            System.out.println(
                    "Unable to open file '" +
                            BILL_FILE_NAME + "'");
        }
    }

    public static int getCurrentBillID(){
        try {
            Scanner scanner = new Scanner(new File(BILL_ID_FILE_NAME));
            String line = scanner.nextLine();
            id = scanner.nextInt();
        }
        catch (Exception ex){
            System.out.println("No file found!");
        }
        return id;
    }

    public static void incrementBillID(){
        try{
            id++;
            PrintWriter writer = new PrintWriter(BILL_ID_FILE_NAME);
            writer.write("ID\n" + id);
            writer.close();
        }
        catch (Exception ex){
            System.out.println(
                    "Unable to open file '" +
                            BILL_ID_FILE_NAME + "'");
        }
    }

    public static ArrayList<Bill> getBills(){
        ArrayList<Bill> bills = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(new File(BILL_FILE_NAME));

            String line = scanner.nextLine();

            while (scanner.hasNext()) {
                Bill bill = new Bill();
                bill.setId(scanner.nextInt());
                bill.setCashier(new UserDBLayer().getUserByName(scanner.next()));
                bill.setTotalPrice(scanner.nextDouble());
                bill.setNrItems(scanner.nextInt());
                bill.setcDate(new ASUtilDate(scanner.next()));
                bills.add(bill);
            }
        }
        catch (Exception ex) {
            System.out.println(
                    "Unable to open file '" +
                            BILL_FILE_NAME + "'");
            ex.printStackTrace();
        }
        return bills;
    }
}
