package com.dblayer;

import com.model.exception.DBException;
import com.model.product.Product;
import com.model.supplier.Supplier;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class SupplierDBLayer {
    private final String fileName = "other\\db\\supplier";

    public Supplier getSupplierByName(String filter) throws DBException {
        Supplier supplier = new Supplier();
        boolean productFound = false;
        try {
            Scanner scanner = new Scanner(new File(fileName));

            String line = scanner.nextLine();
            String name, mobile;
            int id;

            while (scanner.hasNext()) {
                id = scanner.nextInt();
                name = scanner.next();
                mobile = scanner.next();
                if(name.equals(filter)){
                    supplier.setId(id);
                    supplier.setName(name);
                    supplier.setMobileNo(mobile);
                    productFound = true;
                    break;
                }
            }
        } catch (Exception ex) {
            System.out.println(
                    "Unable to open file '" +
                            fileName + "'");
        }
        if(!productFound){
            throw new DBException("Product not in stock!");
        }
        return supplier;
    }

    public ArrayList<Supplier> getSuppliers() {
        ArrayList<Supplier> suppliers = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(new File(fileName));

            String line = scanner.nextLine();

            while (scanner.hasNext()) {
                Supplier supplier = new Supplier();
                supplier.setId(Integer.parseInt(scanner.next()));
                supplier.setName(scanner.next());
                supplier.setMobileNo(scanner.next());
                suppliers.add(supplier);
            }
        }
        catch (Exception ex) {
            System.out.println(
                    "Unable to open file '" +
                            fileName + "'");
            ex.printStackTrace();
        }
        return suppliers;
    }
}
