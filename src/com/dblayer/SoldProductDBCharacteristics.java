package com.dblayer;

import com.model.product.SoldProduct;

import java.util.ArrayList;

public interface SoldProductDBCharacteristics {
    public void saveSoldProduct(SoldProduct soldProduct);
    public ArrayList<SoldProduct> getSoldProductsByBillID();
    public ArrayList<SoldProduct> getSoldProducts();
    public ArrayList<SoldProduct> getSoldProductsByDate(String date1, String date2);
}
