package com.dblayer;

import com.commons.date.ASUtilDate;
import com.model.employee.User;
import com.model.exception.DBException;
import com.model.product.Product;
import com.model.product.SoldProduct;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class UserDBLayer {
    private final String fileName = "other\\db\\user.txt";
    private final String fileNameBinary = "other\\binarydb\\user.dat";

    public void deleteUser(User user){
        try{
            ArrayList<User> users = getUsers();
            for (User user1 : users) {
                if(user.getName().equals(user1.getName())){
                    users.remove(user);
                    break;
                }
            }

            FileWriter fw = new FileWriter(fileName, false);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter writer = new PrintWriter(bw);
            writer.write("NAME USERNAME PASSWORD ACCESSLEVEL SOLDBILLS SOLDITEMS MONEY BIRTHDAY MOBILENO EMAIL SALARY\n");
            for(User user1: users){
                writer.write("\n" + user1.getName() + " ");
                writer.write(user1.getUsername() + " ");
                writer.write(user1.getPassword() + " ");
                writer.write(user1.getAccessLevel() + " ");
                writer.write(user1.getSoldBills() + " ");
                writer.write(user1.getSoldItems() + " ");
                writer.write(user1.getMoney() + " ");
                writer.write(user1.getBirthday().toFileDate() + " ");
                writer.write(user1.getMobileNo() + " ");
                writer.write(user1.getEmail() + " ");
                writer.write(user1.getSalary() + " ");
            }
            writer.close();
        }
        catch (Exception ex){
            System.out.println(
                    "Unable to open file '" +
                            fileName + "'");
        }
    }

    public void updateUser(User user){
        try{
            ArrayList<User> users = getUsers();
            for (User user1 : users) {
                if(user.getName().equals(user1.getName())){
                    user1.setBirthday(user.getBirthday());
                    user1.setAccessLevel(user.getAccessLevel());
                    user1.setEmail(user.getEmail());
                    user1.setMobileNo(user.getMobileNo());
                    user1.setSalary(user.getSalary());
                }
            }

            FileWriter fw = new FileWriter(fileName, false);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter writer = new PrintWriter(bw);
            writer.write("NAME USERNAME PASSWORD ACCESSLEVEL SOLDBILLS SOLDITEMS MONEY BIRTHDAY MOBILENO EMAIL SALARY\n");
            for(User user1: users){
                writer.write("\n" + user1.getName() + " ");
                writer.write(user1.getUsername() + " ");
                writer.write(user1.getPassword() + " ");
                writer.write(user1.getAccessLevel() + " ");
                writer.write(user1.getSoldBills() + " ");
                writer.write(user1.getSoldItems() + " ");
                writer.write(user1.getMoney() + " ");
                writer.write(user1.getBirthday().toFileDate() + " ");
                writer.write(user1.getMobileNo() + " ");
                writer.write(user1.getEmail() + " ");
                writer.write(user1.getSalary() + " ");
            }
            writer.close();
        }
        catch (Exception ex){
            System.out.println(
                    "Unable to open file '" +
                            fileName + "'");
        }
    }

    public void saveUserBinary(User user){

        try{
            DataOutputStream output = new DataOutputStream(new FileOutputStream(fileNameBinary, true));

            output.writeBytes(user.getName());
            output.writeBytes(user.getUsername());
            output.writeUTF(user.getPassword());
            output.writeInt(user.getAccessLevel());
            output.writeInt(user.getSoldBills());
            output.writeInt(user.getSoldItems());
            output.writeDouble(user.getMoney());
            output.writeUTF(user.getBirthday().toFileDate());
            output.writeUTF(user.getMobileNo());
            output.writeUTF(user.getEmail());
            output.writeDouble(user.getSalary());

            output.close();
        }
        catch (Exception ex){
            System.out.println(
                    "Unable to open file '" +
                            fileName + "'");
        }
    }


    public ArrayList<User> getUsersBinary() {
        ArrayList<User> users = new ArrayList<>();
        try {
            DataInputStream input = new DataInputStream(new FileInputStream(fileNameBinary));

            while (true) {
                User user = new User();
                user.setName(input.readUTF());
                user.setUsername(input.readUTF());
                user.setPassword(input.readUTF());
                user.setAccessLevel(input.readInt());
                user.setSoldBills(input.readInt());
                user.setSoldItems(input.readInt());
                user.setMoney(input.readDouble());
                user.setBirthday(new ASUtilDate(input.readUTF()));
                user.setMobileNo(input.readUTF());
                user.setEmail(input.readUTF());
                user.setSalary(input.readDouble());
                users.add(user);
            }
        }
        catch (Exception ex) {
            System.out.println("All data read!");
//            System.out.println(
//                    "Unable to open file '" +
//                            fileName + "'");
        }
        return users;
    }


    public ArrayList<User> getUsers() {
        ArrayList<User> users = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(new File(fileName));

            String line = scanner.nextLine();
            while (scanner.hasNext()) {
                User user = new User();
                user.setName(scanner.next());
                user.setUsername(scanner.next());
                user.setPassword(scanner.next());
                user.setAccessLevel(scanner.nextInt());
                user.setSoldBills(scanner.nextInt());
                user.setSoldItems(scanner.nextInt());
                user.setMoney(scanner.nextDouble());
                user.setBirthday(new ASUtilDate(scanner.next()));
                user.setMobileNo(scanner.next());
                user.setEmail(scanner.next());
                user.setSalary(scanner.nextDouble());
                users.add(user);
            }
        }
        catch (Exception ex) {
            System.out.println(
                    "Unable to open file '" +
                            fileName + "'");
            ex.printStackTrace();
        }
        return users;
    }

    public void saveUser(User user){

        try{
            FileWriter fw = new FileWriter(fileName, true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter writer = new PrintWriter(bw);

            writer.write("\n" + user.getName() + " ");
            writer.write(user.getUsername() + " ");
            writer.write(user.getPassword() + " ");
            writer.write(user.getAccessLevel() + " ");
            writer.write(user.getSoldBills() + " ");
            writer.write(user.getSoldItems() + " ");
            writer.write(user.getMoney() + " ");
            writer.write(user.getBirthday().toFileDate() + " ");
            writer.write(user.getMobileNo() + " ");
            writer.write(user.getEmail() + " ");
            writer.write(user.getSalary() + " ");

            writer.close();
        }
        catch (Exception ex){
            System.out.println(
                    "Unable to open file '" +
                            fileName + "'");
        }
    }


    public User getUserByUsernameBinary(String filter) throws DBException {
        User user = new User();
        boolean userFound = false;
        try {
            DataInputStream input = new DataInputStream(new FileInputStream(fileNameBinary));

            String name, username, password;
            int accessLevel, soldBills, soldItems;
            double money;
            String birthday, mobileNo, email;
            double salary;

            while (true) {
                name = input.readUTF();
                username = input.readUTF();
                password = input.readUTF();
                accessLevel = input.readInt();
                soldBills = input.readInt();
                soldItems = input.readInt();
                money = input.readDouble();
                birthday = input.readUTF();
                mobileNo = input.readUTF();
                email = input.readUTF();
                salary = input.readDouble();
                if(username.equals(filter)){
                    user.setName(name);
                    user.setUsername(username);
                    user.setPassword(password);
                    user.setAccessLevel(accessLevel);
                    user.setSoldBills(soldBills);
                    user.setSoldItems(soldItems);
                    user.setMoney(money);
                    user.setBirthday(new ASUtilDate(birthday));
                    user.setMobileNo(mobileNo);
                    user.setEmail(email);
                    user.setSalary(salary);
                    userFound = true;
                    break;
                }
            }
        } catch (Exception ex) {
            System.out.println(
                    "Unable to open file '" +
                            fileName + "'");
        }
        if(!userFound){
            throw new DBException("User does not exist!");
        }
        return user;
    }


    public User getUserByUsername(String filter) throws DBException {
        User user = new User();
        boolean userFound = false;
        try {
            Scanner scanner = new Scanner(new File(fileName));

            String line = scanner.nextLine();
            String name, username, password;
            int accessLevel, soldBills, soldItems;
            double money;
            String birthday, mobileNo, email;
            double salary;

            while (scanner.hasNext()) {
                name = scanner.next();
                username = scanner.next();
                password = scanner.next();
                accessLevel = scanner.nextInt();
                soldBills = scanner.nextInt();
                soldItems = scanner.nextInt();
                money = scanner.nextDouble();
                birthday = scanner.next();
                mobileNo = scanner.next();
                email = scanner.next();
                salary = scanner.nextDouble();
                if(username.equals(filter)){
                    user.setName(name);
                    user.setUsername(username);
                    user.setPassword(password);
                    user.setAccessLevel(accessLevel);
                    user.setSoldBills(soldBills);
                    user.setSoldItems(soldItems);
                    user.setMoney(money);
                    user.setBirthday(new ASUtilDate(birthday));
                    user.setMobileNo(mobileNo);
                    user.setEmail(email);
                    user.setSalary(salary);
                    userFound = true;
                    break;
                }
            }
        } catch (Exception ex) {
            System.out.println(
                    "Unable to open file '" +
                            fileName + "'");
        }
        if(!userFound){
            throw new DBException("User does not exist!");
        }
        return user;
    }

    public User getUserByName(String filter) throws DBException{
        User user = new User();
        boolean userFound = false;
        try {
            Scanner scanner = new Scanner(new File(fileName));

            String line = scanner.nextLine();
            String name, username, password;
            int accessLevel, soldBills, soldItems;
            double money;
            String birthday, mobileNo, email;
            double salary;


            while (scanner.hasNext()) {
                name = scanner.next();
                username = scanner.next();
                password = scanner.next();
                accessLevel = scanner.nextInt();
                soldBills = scanner.nextInt();
                soldItems = scanner.nextInt();
                money = scanner.nextDouble();
                birthday = scanner.next();
                mobileNo = scanner.next();
                email = scanner.next();
                salary = scanner.nextDouble();
                if(name.equals(filter)){
                    user.setName(name);
                    user.setUsername(username);
                    user.setPassword(password);
                    user.setAccessLevel(accessLevel);
                    user.setSoldBills(soldBills);
                    user.setSoldItems(soldItems);
                    user.setMoney(money);
                    user.setBirthday(new ASUtilDate(birthday));
                    user.setMobileNo(mobileNo);
                    user.setEmail(email);
                    user.setSalary(salary);
                    userFound = true;
                    break;
                }
            }
        } catch (Exception ex) {
            System.out.println(
                    "Unable to open file '" +
                            fileName + "'");
        }
        if(!userFound){
            user = null;
        }

        return user;
    }


}
