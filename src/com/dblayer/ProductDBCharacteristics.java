package com.dblayer;

import com.model.exception.DBException;
import com.model.product.Product;

import java.util.ArrayList;

public interface ProductDBCharacteristics {
    public void updateProduct(Product product);
    public  Product getProductByName(String filter) throws DBException;
    public ArrayList<Product> getProducts();
    public ArrayList<Product> getProductsByDate(String date1, String date2);
}
