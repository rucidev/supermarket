package com.model.bill;

import com.commons.date.ASUtilDate;
import com.commons.entity.Entity;
import com.model.employee.User;
import com.model.product.SoldProduct;

import java.util.ArrayList;

public class Bill extends Entity {
    private User cashier;
    private ArrayList<SoldProduct> products = null;
    private int nrItems = 0;
    private double totalPrice = 0;

    public User getCashier() {
        return cashier;
    }

    public void setCashier(User cashier) {
        this.cashier = cashier;
    }

    public ArrayList<SoldProduct> getProducts() {
        if(products == null){
            products = new ArrayList<SoldProduct>();
        }
        return products;
    }

    public void setProducts(ArrayList<SoldProduct> products) {
        this.products = products;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getNrItems() {
        return nrItems;
    }

    public void setNrItems(int nrItems) {
        this.nrItems = nrItems;
    }
}
