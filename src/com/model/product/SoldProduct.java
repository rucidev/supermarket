package com.model.product;

import com.commons.date.ASUtilDate;
import com.model.product.Product;

public class SoldProduct extends Product {
    private int billID = MINUS_ONE;
    private int soldQuantity = 0;
    private ASUtilDate soldDate = null;

    public int getBillID() {
        return billID;
    }
    public void setBillID(int billID) {
        this.billID = billID;
    }

    public ASUtilDate getSoldDate() {
        return soldDate;
    }

    public void setSoldDate(ASUtilDate soldDate) {
        this.soldDate = soldDate;
    }

    public int getSoldQuantity() {
        return soldQuantity;
    }

    public void setSoldQuantity(int soldQuantity) {
        this.soldQuantity = soldQuantity;
    }
}
