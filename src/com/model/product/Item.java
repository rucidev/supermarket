package com.model.product;

import com.commons.entity.Entity;

import java.util.ArrayList;

public class Item extends Entity{
    private int categoryID = MINUS_ONE;
    private ArrayList<Product> products = null;

    public ArrayList<Product> getProducts() {
        if(products == null){
            products = new ArrayList<>();
        }
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }
}
