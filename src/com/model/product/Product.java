package com.model.product;

import com.commons.date.ASUtilDate;
import com.commons.entity.Entity;

public class Product extends Entity{
    private String name = "";
    private int supplierID = MINUS_ONE;
    private ASUtilDate purchasedDate = null;
    private double purchasedPrice = 0;
    private int quantity = 0;
    private double sellingPrice = 0;
    private double boughtPrice = 0;
    private ASUtilDate boughtDate = null;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSupplierID() {
        return supplierID;
    }

    public void setSupplierID(int supplierID) {
        this.supplierID = supplierID;
    }

    public ASUtilDate getPurchasedDate() {
        return purchasedDate;
    }

    public void setPurchasedDate(ASUtilDate purchasedDate) {
        this.purchasedDate = purchasedDate;
    }

    public double getPurchasedPrice() {
        return purchasedPrice;
    }

    public void setPurchasedPrice(double purchasedPrice) {
        this.purchasedPrice = purchasedPrice;
    }

    public ASUtilDate getBoughtDate() {
        return boughtDate;
    }

    public void setBoughtDate(ASUtilDate boughtDate) {
        this.boughtDate = boughtDate;
    }

    public double getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(double sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getBoughtPrice() {
        return boughtPrice;
    }

    public void setBoughtPrice(double boughtPrice) {
        this.boughtPrice = boughtPrice;
    }
}
