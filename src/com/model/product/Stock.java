package com.model.product;

import java.util.HashSet;
import java.util.Set;

public class Stock {
    private Set<Item> items = null;

    public Set<Item> getItems() {
        if(items == null){
            items = new HashSet<Item>();
        }
        return items;
    }

    public void setItems(Set<Item> items) {
        this.items = items;
    }
}
