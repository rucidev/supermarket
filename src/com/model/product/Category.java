package com.model.product;

import com.commons.entity.Entity;

import java.util.ArrayList;

public class Category extends Entity {
    private String name = "";
    private ArrayList<Item> items = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Item> getProducts() {
        if(items == null){
            items = new ArrayList<Item>();
        }
        return items;
    }

    public void setProducts(ArrayList<Item> products) {
        this.items = products;
    }
}
