package com.model.product;

import com.commons.date.ASUtilDate;

public class BoughtProduct extends Product{
    private int supplierID = MINUS_ONE;
//    private int quantity = 0;
    private ASUtilDate boughtDate = null;

    @Override
    public int getSupplierID() {
        return supplierID;
    }

    @Override
    public void setSupplierID(int supplierID) {
        this.supplierID = supplierID;
    }

//    public int getQuantity() {
//        return quantity;
//    }
//
//    public void setQuantity(int quantity) {
//        this.quantity = quantity;
//    }

    public ASUtilDate getBoughtDate() {
        return boughtDate;
    }

    public void setBoughtDate(ASUtilDate boughtDate) {
        this.boughtDate = boughtDate;
    }
}
