package com.model.employee;

public class MobileNo {
    private String mobileNoPrefix = "";
    private String mobileNo = "";

    public MobileNo(){
        //default constructor
    }

    public MobileNo(String mobileNoPrefix, String mobileNo){
        mobileNoPrefix = mobileNoPrefix;
        mobileNo = mobileNo;
    }

    public String getMobileNoPrefix() {
        return mobileNoPrefix;
    }

    public void setMobileNoPrefix(String mobileNoPrefix) {
        this.mobileNoPrefix = mobileNoPrefix;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }
}
