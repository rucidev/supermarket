package com.model.employee;

import com.commons.date.ASUtilDate;
import com.commons.entity.Entity;

public class Employee extends Entity {
    public static final int ADMINISTRATOR = 2;
    public static final int ECONOMIST = 1;
    public static final int CASHIER = 0;

    private String name = "";
    private String surname = "";
    private ASUtilDate birthday = null;
    private MobileNo mobileNo = null;
    private String email = "";
    private double salary = 0;
    private User user = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public ASUtilDate getBirthday() {
        return birthday;
    }

    public void setBirthday(ASUtilDate birthday) {
        this.birthday = birthday;
    }

    public MobileNo getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(MobileNo mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
