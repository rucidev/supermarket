package com.model.employee;

import com.commons.date.ASUtilDate;
import com.commons.entity.Entity;

public class User extends Entity{


    private String name = "";
    private String username = "";
    private String password = "";
    private int accessLevel = MINUS_ONE;
    private int soldBills = 0;
    private int soldItems = 0;
    private double money = 0;
    private ASUtilDate birthday = null;
    private String mobileNo = "";
    private String email = "";
    private double salary = 0;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(int accessLevel) {
        this.accessLevel = accessLevel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSoldBills() {
        return soldBills;
    }

    public void setSoldBills(int soldBills) {
        this.soldBills = soldBills;
    }

    public int getSoldItems() {
        return soldItems;
    }

    public void setSoldItems(int soldItems) {
        this.soldItems = soldItems;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public ASUtilDate getBirthday() {
        return birthday;
    }

    public void setBirthday(ASUtilDate birthday) {
        this.birthday = birthday;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
}
