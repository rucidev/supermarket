package com.model.exception;

public class DBException extends Exception{
    public DBException(){
        // default constructor
    }

    public DBException(String message){
        super(message);
    }
}
