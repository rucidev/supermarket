package com.model.supplier;

import com.commons.entity.Entity;
import com.model.employee.MobileNo;
import com.model.product.Product;

import java.util.ArrayList;

public class Supplier extends Entity{
    private String name = "";
    private String mobileNo = "";
    private ArrayList<Product> products = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

}
