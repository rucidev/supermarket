package com.utils;

import com.commons.date.ASUtilDate;
import com.dblayer.UserDBLayer;
import com.model.employee.User;

import java.util.ArrayList;

public class TestBinaryFiles {
    public static void main(String[] args) {
        try {
            User user = new User();
            user.setName("igli");
            user.setUsername("a");
            user.setPassword("a");
            user.setSalary(60000);
            user.setMobileNo("0698383923");
            user.setEmail("igli@epoka.edu.al");
            user.setAccessLevel(1);
            user.setBirthday(new ASUtilDate("1980-10-05"));
            user.setSoldItems(0);
            user.setSoldBills(0);
            user.setMoney(0);

            new UserDBLayer().saveUserBinary(user);
            ArrayList<User> users = new UserDBLayer().getUsersBinary();
            for(int i = 0; i< users.size(); i++){
                System.out.println(user.getName());
                System.out.println(user.getUsername());
                System.out.println(user.getPassword());
                System.out.println(user.getAccessLevel());
                System.out.println(user.getBirthday().toFileDate());
                System.out.println(user.getEmail());
                System.out.println(user.getMobileNo());
                System.out.println(user.getSalary());
                System.out.println(user.getMoney());
                System.out.println(user.getSoldBills());
                System.out.println(user.getSoldItems());
            }
        }catch (Exception ex){

        }
    }
}
