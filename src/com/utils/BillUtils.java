package com.utils;

import com.commons.date.ASUtilDate;
import com.model.bill.Bill;
import com.model.product.SoldProduct;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;

public class BillUtils {
    private static final String DESTINATION = "other\\bills\\Bill";

    public static void printBill(Bill bill){
        int id = bill.getId();
        String cashier = bill.getCashier().getUsername();
        double totalPrice = bill.getTotalPrice();
        ArrayList<SoldProduct> products = bill.getProducts();
        bill.setcDate(new ASUtilDate());

        String fileName = DESTINATION + id;
        try {
            PrintWriter writer = new PrintWriter(fileName);
            writer.write("Cashier: " + cashier + " Date printed: " + bill.getcDate() + "\n");
            for (SoldProduct product : products) {
                String productText = "ID: " + product.getId() + "   Name:" + product.getName() + "    Price:" + product.getSellingPrice() + "   Quantity: " + product.getSoldQuantity() +
                        "    | " + product.getSellingPrice() * product.getSoldQuantity() + "\n";

                writer.write(productText);
            }
            writer.write("\n TOTAL : " + totalPrice);
            writer.write("\n Bill ID: " + id);
            writer.close();
        }
        catch (Exception ex){
            System.out.println(fileName + " not found!");
            ex.printStackTrace();
        }

    }
}
