package com.gui.economist;

import com.commons.date.ASUtilDate;
import com.dblayer.*;
import com.gui.AlertGUI;
import com.model.bill.Bill;
import com.model.employee.User;
import com.model.product.Product;
import com.model.product.SoldProduct;
import com.model.supplier.Supplier;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

import java.util.ArrayList;

public class EconomistMenuHandler {
    static GridPane mainPane = null;
    static GridPane supplyPane = new GridPane();
    static GridPane cashierPane = new GridPane();
    static GridPane productPane = new GridPane();


    public static void handleListButtonOnAction(Button listButton, ComboBox<String> productsSoldCombo, ComboBox<String> productsBoughtCombo, TextField firstTf, TextField secondTf, TextField revenueTf){
        listButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                double profit = fillSoldProductsComobBox(productsSoldCombo, firstTf, secondTf);
                double cost = fillBoughtProductsComboBox(productsBoughtCombo, firstTf, secondTf);
                double total = profit - cost;
                revenueTf.setText("Total revenue: " + total);
            }
        });
    }

    public static double fillBoughtProductsComboBox(ComboBox<String> productsBoughtCombo, TextField firstTf, TextField secondTf) {
        double res = 0;

        ArrayList<Product> boughtProducts = new ProductDBLayer().getProductsByDate(firstTf.getText(), secondTf.getText());
        for (Product product: boughtProducts) {
            productsBoughtCombo.getItems().add(product.getName());
            res -= product.getBoughtPrice();
        }
        return res;
    }

    public static double fillSoldProductsComobBox(ComboBox<String> productsSoldCombo, TextField firstTf, TextField secondTf) {
        double res = 0;

        ArrayList<SoldProduct> soldProducts = new SoldProductDBLayer().getSoldProductsByDate(firstTf.getText(), secondTf.getText());
        for (SoldProduct soldProduct: soldProducts) {
            productsSoldCombo.getItems().add(soldProduct.getName());
            res += soldProduct.getSoldQuantity();
        }
        return res;
    }

    public static void handleOkButtonCashierOnAction(Button okButton, ComboBox<String> cashierComboBox, TextField noBillsTf, TextField itemsSoldTf, TextField moneyTf, TextField firstDateTf, TextField secondDateTf){
        okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    buttonComboBoxHandlerMethod(cashierComboBox, firstDateTf, secondDateTf, noBillsTf, itemsSoldTf, moneyTf);
                }catch (Exception ex){
                    System.out.println(ex.getMessage());
                }

            }
        });
    }

    public static void handleCashierComboBxOnAction(ComboBox<String> cashierComboBox, TextField noBillsTf, TextField itemsSoldTf, TextField moneyTf, TextField firstDateTf, TextField secondDateTf){
        cashierComboBox.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    buttonComboBoxHandlerMethod(cashierComboBox, firstDateTf, secondDateTf, noBillsTf, itemsSoldTf, moneyTf);
                }catch (Exception ex){
                    System.out.println(ex.getMessage());
                }

            }
        });
    }

    private static void buttonComboBoxHandlerMethod(ComboBox<String> cashierComboBox, TextField firstDateTf, TextField secondDateTf, TextField noBillsTf, TextField itemsSoldTf, TextField moneyTf) throws Exception {
        int noBills = 0, nrItems = 0;
        double totalPrice = 0;
        String userName = cashierComboBox.getValue();
        User user = new UserDBLayer().getUserByName(userName);
        ArrayList<Bill> bills = BillDBLayer.getBills();
        for (Bill bill : bills) {
            if(bill.getCashier().getName().equals(user.getName())){
                ASUtilDate firstDate = new ASUtilDate(firstDateTf.getText());
                ASUtilDate secondDate = new ASUtilDate(secondDateTf.getText());
                ASUtilDate billDate = bill.getcDate();
                int compareRes = billDate.toFileDate().compareTo(firstDate.toFileDate());
                int secondCompareRes = billDate.toFileDate().compareTo(secondDate.toFileDate());
                if(compareRes != -1){
                    if(secondCompareRes != 1){
                        noBills++;
                        nrItems += bill.getNrItems();
                        totalPrice += bill.getTotalPrice();
                    }
                }
            }
        }
        noBillsTf.setText(noBills + "");
        itemsSoldTf.setText(nrItems + "");
        moneyTf.setText(totalPrice + "");
    }

    public static void handleProductsButtonOnAction(Button productsButton){
        productsButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    removePreviousPanes();
                    productPane = EconomistProductPaneGUI.getPane();
                    mainPane.add(productPane, 0, 2);
                }
                catch (Exception ex){
                    System.out.println(ex.getMessage());
                }
            }
        });
    }

    private static void removePreviousPanes() {
        mainPane.getChildren().remove(supplyPane);
        mainPane.getChildren().remove(cashierPane);
        mainPane.getChildren().remove(productPane);
    }

    public static void handleCashiersButtonOnAction(Button cashierButton){
        cashierButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    removePreviousPanes();
                    cashierPane = EconomistCashierPaneGUI.getPane();
                    mainPane.add(cashierPane, 0, 2);
                }
                catch (Exception ex){
                    System.out.println(ex.getMessage());
                }
            }
        });
    }

    public static void handleSupplyButtonOnAction(Button supplyButton){
        supplyButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    removePreviousPanes();
                    supplyPane.setPadding(new Insets(50, 0, 50, 0));
                    supplyPane.add(EconomistSupplyPaneGUI.getPane(), 0, 0);
                    supplyPane.add(EconomistSupplyPaneGUI.getListOfProductPane(new ArrayList<>()), 1, 0);
                    mainPane.add(supplyPane, 0, 2);
                }
                catch (Exception ex){
                    System.out.println(ex.getMessage());
                }
            }
        });
    }

    public static void handleOkButtonOnAction(Button okButton, TextField idTf, TextField quantityTf){
        okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    Product product = new ProductDBLayer().getProductById(Integer.parseInt(idTf.getText()));
                    product.setQuantity(product.getQuantity() + Integer.parseInt(quantityTf.getText()));
                    new ProductDBLayer().updateProduct(product);
                    AlertGUI.showAlertMessage("Success", "Successfully bought " + quantityTf.getText() + " items of " + product.getName() + "!");
                }
                catch (Exception ex){
                    AlertGUI.showAlertMessage("Error", "Please enter the right information!");
                    System.out.println(ex.getMessage());
                }
            }
        });
    }

    public static void handleCashiersComboOnAction(ComboBox<String> cashiers){
        cashiers.setOnAction((event) -> {
            try {
                String userName = cashiers.getValue();
                User user = new UserDBLayer().getUserByName(userName);

            }
            catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        });
    }

    public static void handleSuppliersComboOnAction(ComboBox<String> suppliers){
        suppliers.setOnAction((event) -> {
            try {
                String supplierName = suppliers.getValue();
                Supplier supplier = new SupplierDBLayer().getSupplierByName(supplierName);
                ArrayList<Product> products = new ProductDBLayer().getProductsBySupplierID(supplier.getId());
                supplyPane.add(EconomistSupplyPaneGUI.getPaneWithButtons(supplierName), 0, 0);
                supplyPane.add(EconomistSupplyPaneGUI.getListOfProductPane(products), 1, 0);
                mainPane.add(supplyPane, 0, 2);
            }
            catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        });
    }

    public GridPane getMainPane() {
        return mainPane;
    }

    public void setMainPane(GridPane mainPane) {
        this.mainPane = mainPane;
    }

    public GridPane getCashierPane() {
        return cashierPane;
    }

    public void setCashierPane(GridPane cashierPane) {
        this.cashierPane = cashierPane;
    }

    public static GridPane getProductPane() {
        return productPane;
    }

    public static void setProductPane(GridPane productPane) {
        EconomistMenuHandler.productPane = productPane;
    }
}
