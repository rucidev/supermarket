package com.gui.economist;

import com.model.product.Product;
import com.model.supplier.Supplier;
import com.dblayer.SupplierDBLayer;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

import java.util.ArrayList;

public class EconomistSupplyPaneGUI {

    public static GridPane getPane(){
        GridPane gridPane = new GridPane();

        gridPane.add(getSuppliersComboBox(), 0, 0);

        gridPane.setVgap(10);
        gridPane.setHgap(10);
        gridPane.setPadding(new Insets(0, 40, 0, 20));

        return gridPane;
    }

    public static GridPane getPaneWithButtons(String supplier){
        GridPane gridPane = getPane();
        TextField supplierTf = new TextField(supplier);
        supplierTf.setEditable(false);
        gridPane.add(supplierTf, 0, 1);
        gridPane.add(getMenuButtons(), 0, 2);
        return gridPane;
    }

    private static HBox getMenuButtons(){
        HBox hBox = new HBox(10);
        hBox.setPadding(new Insets(0,0,0,0));

        TextField idTf = new TextField("ID");
        idTf.setMinWidth(30);
        TextField quantityTf = new TextField("Quantity");
        quantityTf.setMinWidth(60);
        Button okButton = new Button("OK");
        EconomistMenuHandler.handleOkButtonOnAction(okButton, idTf, quantityTf);

        hBox.getChildren().addAll(idTf, quantityTf, okButton);
        return hBox;
    }

    private static ComboBox<String> getSuppliersComboBox(){
        ComboBox<String> suppliersCombo = new ComboBox<>();
        suppliersCombo.setPadding(new Insets(0,0,0,30));
        ArrayList<Supplier> suppliers = new SupplierDBLayer().getSuppliers();

        suppliersCombo.setMinWidth(350);
        suppliersCombo.setValue("Supplier");

        for (Supplier supplier: suppliers) {
            suppliersCombo.getItems().add(supplier.getName());
        }
        EconomistMenuHandler.handleSuppliersComboOnAction(suppliersCombo);
        return suppliersCombo;
    }

    public static GridPane getListOfProductPane(ArrayList<Product> products){
        GridPane pane = new GridPane();
        pane.setStyle("-fx-background-color: white");

        TextField nameTf = new TextField("ID:                                               Name:                                          Price:");
        nameTf.setMinWidth(510);
        nameTf.setStyle("-fx-background-color: #ffcaca");
        pane.add(nameTf, 0, 0, 3, 1);
        for (int i = 0 ; i < products.size(); i++) {
            Product product = products.get(i);

            pane.add(getNotEditableTextField(product.getId() + ""), 0, i+1);
            pane.add(getNotEditableTextField(product.getName()), 1, i+1);
            pane.add(getNotEditableTextField(product.getBoughtPrice() + ""), 2, i+1);
        }
        return pane;
    }

    private static TextField getNotEditableTextField(String name) {
        TextField tf = new TextField(name);
        tf.setEditable(false);
        tf.setMinWidth(170);
        return tf;
    }
}
