package com.gui.economist;

import com.dblayer.ProductDBLayer;
import com.gui.AlertGUI;
import com.gui.SupermarketGUIAbstract;
import com.gui.cashier.CashierMenuHandler;
import com.model.employee.User;
import com.model.product.Product;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.ArrayList;

public class EconomistMenuGUI extends SupermarketGUIAbstract{
    private Stage primaryStage = null;
    private User user = null;
    private EconomistMenuHandler economistMenuGUI = new EconomistMenuHandler();

    public EconomistMenuGUI(User user){
        this.user = user;
    }

    public void start(){
        primaryStage = new Stage();
        GridPane gridPane = new GridPane();

        addHeaderToMainPane(gridPane, primaryStage, user);
        addMenuBarToMainPane(gridPane);

        gridPane.setStyle("-fx-background-color: #fffae1");
        gridPane.setPadding(new Insets(20, 20, 20, 20));
        primaryStage.setTitle("Supermarket / Cashier");
        primaryStage.setScene(new Scene(gridPane, 1000, 500));
        ArrayList<Product> products = new ProductDBLayer().getProducts();
        String productNames = "";
        boolean alert = false;
        for(int i= 0 ;i < products.size(); i++){
            if(products.get(i).getQuantity() <= 5){
                productNames += products.get(i).getName() + ", ";
                alert = true;
            }
        }

        primaryStage.show();
        if(alert){
            AlertGUI.showAlertMessage("Attention", "Products: " + productNames + " have less than 5 items in stock!");
        }
    }

    private void addMenuBarToMainPane(GridPane gridPane){
        HBox hBox = new HBox();
        hBox.setPadding(new Insets(20,0,0,0));

        Button supplyButton = getMenuButton("Supply");
        Button cashierButton = getMenuButton("Cashiers");
        Button productButton = getMenuButton("Products");

        economistMenuGUI.setMainPane(gridPane);
        economistMenuGUI.handleSupplyButtonOnAction(supplyButton);
        economistMenuGUI.handleCashiersButtonOnAction(cashierButton);
        economistMenuGUI.handleProductsButtonOnAction(productButton);

        hBox.getChildren().addAll(supplyButton, cashierButton, productButton);
        gridPane.add(hBox, 0, 1);
    }

    private Button getMenuButton(String text){
        Button button = new Button(text);
        button.setMinWidth(320);
        return button;
    }

//    private void addMenuBarToMainPane(GridPane pane) {
//        MenuBar menuBar = new MenuBar();
//
//        Menu menu1 = new Menu("Menu");
//
//        MenuItem menuItemA = new MenuItem("Item A");
//        menuItemA.setOnAction(new EventHandler<ActionEvent>() {
//
//            @Override public void handle(ActionEvent e) {
//                System.out.println("Item A Clicked");
//            }
//        });
//
//        MenuItem menuItemB = new MenuItem("Item B");
//        menuItemB.setOnAction(new EventHandler<ActionEvent>() {
//
//            @Override public void handle(ActionEvent e) {
//                System.out.println("Item B Clicked");
//            }
//        });
//
//        MenuItem menuItemC = new MenuItem("Item C");
//        menuItemC.setOnAction(new EventHandler<ActionEvent>() {
//
//            @Override public void handle(ActionEvent e) {
//                System.out.println("Item C Clicked");
//            }
//        });
//
//        menu1.getItems().add(menuItemA);
//        menu1.getItems().add(menuItemB);
//        menu1.getItems().add(menuItemC);
//        menuBar.getMenus().add(menu1);
//
//        menuBar.prefWidthProperty().bind(primaryStage.widthProperty());
//        pane.add(menuBar, 0, 1);
//    }
}
