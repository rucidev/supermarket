package com.gui.economist;

import com.commons.date.ASUtilDate;
import com.dblayer.UserDBLayer;
import com.gui.SupermarketGUIAbstract;
import com.model.employee.User;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.ArrayList;

public class EconomistCashierPaneGUI{
    private static TextField nrBillsTf = new TextField();
    private static TextField itemsSoldTf = new TextField();
    private static TextField moneyMadeTf = new TextField();
    private static TextField firstDateTf = null;
    private static TextField secondDateTf = null;
    private static ComboBox<String> cashierComboBox = null;

    public static GridPane getPane(){
        GridPane gridPane = new GridPane();

        setCashierComboBoxValues();
        gridPane.add(cashierComboBox, 0, 0);
        gridPane.add(getStats(), 0, 1);
        gridPane.add(getDateTextFields(), 0, 2);
        Button okButton = new Button("OK");
        okButton.setMinWidth(300);
        okButton.setAlignment(Pos.CENTER);
        gridPane.add(okButton, 0, 3);
        EconomistMenuHandler.handleCashierComboBxOnAction(cashierComboBox, nrBillsTf, itemsSoldTf, moneyMadeTf, firstDateTf, secondDateTf);
        EconomistMenuHandler.handleOkButtonCashierOnAction(okButton, cashierComboBox, nrBillsTf, itemsSoldTf, moneyMadeTf, firstDateTf, secondDateTf);


        gridPane.setVgap(10);
        gridPane.setHgap(10);
        gridPane.setPadding(new Insets(30, 335, 0, 330));

        return gridPane;
    }

    private static VBox getStats(){
        VBox vBox = new VBox(10);
        vBox.setPadding(new Insets(30,0,0,0));

        GridPane firstLine = new GridPane();
        Label billsLabel = new Label("No Bills: ");
        billsLabel.setMinWidth(120);
        billsLabel.setPadding(new Insets(0,0,0,30));
        firstLine.add(billsLabel, 0, 0);
        firstLine.add(nrBillsTf, 1, 0);

        GridPane secondLine = new GridPane();
        Label soldLabel = new Label("Items Sold: ");
        soldLabel.setMinWidth(120);
        soldLabel.setPadding(new Insets(0,0,0,30));
        secondLine.add(soldLabel, 0, 0);
        secondLine.add(itemsSoldTf, 1, 0);

        GridPane thirdLine = new GridPane();
        Label moneyLabel = new Label("Money Made: ");
        moneyLabel.setMinWidth(120);
        moneyLabel.setPadding(new Insets(0,0,0,30));
        thirdLine.add(moneyLabel, 0, 0);
        thirdLine.add(moneyMadeTf, 1, 0);

//        GridPane fourthLine = new GridPane();
//        Label moneyLabel = new Label("Money Made: ");
//        moneyLabel.setMinWidth(120);
//        moneyLabel.setPadding(new Insets(0,0,0,30));
//        thirdLine.add(moneyLabel, 0, 0);
//        thirdLine.add(moneyMadeTf, 1, 0);

        vBox.getChildren().addAll(firstLine, secondLine, thirdLine);
        return vBox;
    }

    private static HBox getDateTextFields(){
        HBox hbox = new HBox(10);

        firstDateTf = new TextField(new ASUtilDate().toFileDate());
        secondDateTf = new TextField(new ASUtilDate().toFileDate());

        hbox.getChildren().addAll(firstDateTf, secondDateTf);
        hbox.setPadding(new Insets(20, 0,10, 0));
        return hbox;
    }

    private static void setCashierComboBoxValues(){
        cashierComboBox = new ComboBox<>();
        cashierComboBox.setStyle("-fx-background-color: #ffcaca");
        ArrayList<User> users = new UserDBLayer().getUsers();

        cashierComboBox.setMinWidth(300);
        cashierComboBox.setValue("Cashiers");

        for (User user: users) {
            if(user.getAccessLevel() == 0){
                cashierComboBox.getItems().add(user.getName());
            }
        }
    }

    public static TextField getNrBillsTf() {
        return nrBillsTf;
    }

    public static void setNrBillsTf(TextField nrBillsTf) {
        EconomistCashierPaneGUI.nrBillsTf = nrBillsTf;
    }

    public static TextField getItemsSoldTf() {
        return itemsSoldTf;
    }

    public static void setItemsSoldTf(TextField itemsSoldTf) {
        EconomistCashierPaneGUI.itemsSoldTf = itemsSoldTf;
    }

    public static TextField getMoneyMadeTf() {
        return moneyMadeTf;
    }

    public static void setMoneyMadeTf(TextField moneyMadeTf) {
        EconomistCashierPaneGUI.moneyMadeTf = moneyMadeTf;
    }
}
