package com.gui.economist;

import com.commons.date.ASUtilDate;
import com.dblayer.ProductDBLayer;
import com.dblayer.SoldProductDBLayer;
import com.dblayer.UserDBLayer;
import com.model.employee.User;
import com.model.product.BoughtProduct;
import com.model.product.Product;
import com.model.product.SoldProduct;
import com.model.supplier.Supplier;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

import java.util.ArrayList;

public class EconomistProductPaneGUI {
    private static TextField firstTf = null;
    private static TextField secondTf = null;
    private static ComboBox<String> productsBoughtCombo = null;
    private static ComboBox<String> productsSoldCombo = null;
    private static Button listButton = null;
    private static TextField revenueTf = null;


    public static GridPane getPane(){
        GridPane gridPane = new GridPane();

        gridPane.add(getDateTextFields(), 0, 0);
        gridPane.add(getProductsSoldComboBox(), 0, 1);
        gridPane.add(getTotalField(), 0, 2);


        EconomistMenuHandler.handleListButtonOnAction(listButton, productsSoldCombo, productsBoughtCombo, firstTf, secondTf, revenueTf);
        gridPane.setVgap(10);
        gridPane.setHgap(10);
        gridPane.setMinWidth(200);
        gridPane.setPadding(new Insets(30, 250, 0, 250));

        return gridPane;
    }

    private static VBox getDateTextFields(){
        VBox vBox = new VBox(5);
        listButton = new Button("LIST PRODUCTS");
        listButton.setMinWidth(450);
        HBox hbox = new HBox(10);
        firstTf = new TextField(new ASUtilDate().toFileDate());
        firstTf.setMinWidth(220);
        secondTf = new TextField(new ASUtilDate().toFileDate());
        secondTf.setMinWidth(220);
        hbox.getChildren().addAll(firstTf, secondTf);
        hbox.setPadding(new Insets(10, 0,0, 0));

        vBox.getChildren().addAll(listButton, hbox);
        return vBox;
    }

    private static HBox getTotalField(){
        HBox hbox = new HBox(10);

        revenueTf = new TextField("Total revenue: " );
        revenueTf.setFont(new Font(20));
        revenueTf.setAlignment(Pos.CENTER);
        revenueTf.setMinWidth(450);
        revenueTf.setMinHeight(60);
        hbox.getChildren().addAll(revenueTf);
        hbox.setPadding(new Insets(180, 0,0, 0));
        return hbox;
    }

    private static HBox getProductsSoldComboBox(){
        HBox hbox = new HBox(10);
        productsBoughtCombo = new ComboBox<>();
        productsBoughtCombo.setStyle("-fx-background-color: #ffcaca");
        productsBoughtCombo.setMinWidth(220);
        productsBoughtCombo.setValue("Products Bought");


        productsSoldCombo = new ComboBox<>();
        productsSoldCombo.setStyle("-fx-background-color: #ffcaca");
        productsSoldCombo.setMinWidth(220);
        productsSoldCombo.setValue("Products Sold");


        hbox.getChildren().addAll(productsSoldCombo, productsBoughtCombo);

        return hbox;
    }

}
