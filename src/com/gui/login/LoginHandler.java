package com.gui.login;

import com.gui.AlertGUI;
import com.gui.administrator.AdministratorMenuGUI;
import com.gui.cashier.CashierMenuGUI;
import com.gui.economist.EconomistMenuGUI;
import com.model.employee.User;
import com.dblayer.UserDBLayer;
import com.model.exception.DBException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class LoginHandler {
    User user = null;
    private TextField userTextField = null;
    private PasswordField passwordField = null;
    private Button loginButton = null;
    private int accessLevel = 0;

    public LoginHandler(TextField userTextField, PasswordField passwordField, Button loginButton){
        this.userTextField = userTextField;
        this.passwordField = passwordField;
        this.loginButton = loginButton;
    }

    public void handleLoginButtonOnAction(Stage primaryStage) {
        loginButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                validateLoginInformation(primaryStage);

            }
        });
    }

    public void handleUserTextFieldOnAction(Stage primaryStage) {
        userTextField.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                validateLoginInformation(primaryStage);
            }
        });
    }

    public void handlePasswordTextFieldOnAction(Stage primaryStage) {
        passwordField.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                validateLoginInformation(primaryStage);
            }
        });
    }

    private boolean isValidUser(String username, String password) throws DBException{
        boolean retValue = false;
        UserDBLayer dbLayer = new UserDBLayer();
        user = dbLayer.getUserByUsername(username);
        accessLevel = user.getAccessLevel();
        retValue = user.getUsername().equals(username) && user.getPassword().equals(password);
        System.out.println(password + " " + user.getPassword() + " " + retValue);
        return retValue;
    }

    private void validateLoginInformation(Stage primaryStage) {
        String username = userTextField.getText();
        String password = passwordField.getText();
        try {
            boolean isValidUser = isValidUser(username, password);
            if (!isValidUser){
                AlertGUI.showAlertMessage("Error", "Invalid User information");
            }
            else{
                if(accessLevel == 0){
                    new CashierMenuGUI(user).start();
                }
                else if(accessLevel == 1){
                    new EconomistMenuGUI(user).start();
                }
                else if(accessLevel == 2){
                    new AdministratorMenuGUI(user).start();
                }
                primaryStage.close();
            }
        }catch (DBException exception){
            AlertGUI.showAlertMessage("Error", "User does not exist " );
        }
    }

    public TextField getUserTextField() {
        return userTextField;
    }

    public void setUserTextField(TextField userTextField) {
        this.userTextField = userTextField;
    }

    public PasswordField getPasswordField() {
        return passwordField;
    }

    public void setPasswordField(PasswordField passwordField) {
        this.passwordField = passwordField;
    }

    public Button getLoginButton() {
        return loginButton;
    }

    public void setLoginButton(Button loginButton) {
        this.loginButton = loginButton;
    }
}
