package com.gui.login;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class LoginGUI {
    public void start() {
        Stage primaryStage = new Stage();
        GridPane pane = new GridPane();
        pane.setAlignment(Pos.CENTER);
        pane.setHgap(10);
        pane.setVgap(10);
        pane.setPadding(new Insets(25, 25, 25, 25));
        pane.setAlignment(Pos.CENTER);

        Text memberLoginText = new Text("Member Login");
        memberLoginText.setFont(Font.font("Cascade", FontWeight.NORMAL, 33));
        pane.add(memberLoginText, 0, 0, 2, 1);

        Label usernameLabel = new Label("Username:");
        pane.add(usernameLabel, 0, 1);

        TextField userTextField = new TextField();
        pane.add(userTextField, 1, 1);

        Label pw = new Label("Password:");
        pane.add(pw, 0, 2);

        PasswordField passwordField = new PasswordField();
        pane.add(passwordField, 1, 2);

        Button loginButton = new Button("LOGIN");
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().add(loginButton);
        pane.add(hbBtn, 1, 4);

        LoginHandler loginHandler = new LoginHandler(userTextField, passwordField, loginButton);

        loginHandler.handleUserTextFieldOnAction(primaryStage);
        loginHandler.handlePasswordTextFieldOnAction(primaryStage);
        loginHandler.handleLoginButtonOnAction(primaryStage);

        primaryStage.setTitle("Supermarket");
        primaryStage.setScene(new Scene(pane, 300, 275));
        primaryStage.show();
    }




}