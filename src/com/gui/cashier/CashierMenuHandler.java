package com.gui.cashier;

import com.commons.date.ASUtilDate;
import com.gui.AlertGUI;
import com.gui.login.LoginGUI;
import com.model.bill.Bill;
import com.utils.BillUtils;
import com.dblayer.BillDBLayer;
import com.model.employee.User;
import com.model.exception.DBException;
import com.model.product.Product;
import com.model.product.SoldProduct;
import com.dblayer.ProductDBLayer;
import com.dblayer.SoldProductDBLayer;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

import java.util.ArrayList;


public class CashierMenuHandler {
    private ComboBox<String> names = null;
    private ArrayList<SoldProduct> soldProducts = new ArrayList<>();
    private ArrayList<Product> products = new ArrayList<>();
    private TextField id = null;
    private TextField quantityTf = null;
    private String name = null;
    int row = 0;

    public void handleCancelButton(Button cancel, User user, Stage stage){
        cancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                    new CashierMenuGUI(user).start();
                    stage.close();
            }
        });
    }

    public void handleOkButtonOnAction(Button ok, int id, User cashier, Stage stage){
        ok.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    double totalPrice = 0;

                    for(int i = 0 ; i < products.size(); i++){
                        soldProducts.get(i).setBillID(BillDBLayer.getCurrentBillID());
                        soldProducts.get(i).setSoldDate(new ASUtilDate());
                        new SoldProductDBLayer().saveSoldProduct(soldProducts.get(i));
                        totalPrice += soldProducts.get(i).getSellingPrice() * soldProducts.get(i).getSoldQuantity();
                        products.get(i).setQuantity(products.get(i).getQuantity() - soldProducts.get(i).getSoldQuantity());
                        new ProductDBLayer().updateProduct(products.get(i));
                    }

                    BillUtils.printBill( getBill(totalPrice, id, cashier));
                    BillDBLayer.incrementBillID();
                    BillDBLayer.saveBill(getBill(totalPrice, id, cashier));
                    AlertGUI.showAlertMessage("Success", "Bill NO " + id + " successfully printed in other/bills");
                    stage.close();
                    new CashierMenuGUI(cashier).start();
                }
                catch(Exception e){
                    System.out.println(e.getMessage());
                }

            }
        });
    }

    private Bill getBill(double totalPrice, int id, User cashier) {
        Bill bill = new Bill();
        bill.setProducts(soldProducts);
        bill.setNrItems(soldProducts.size());
        bill.setTotalPrice(totalPrice);
        bill.setId(id);
        bill.setCashier(cashier);
        bill.setcDate(new ASUtilDate());
        return bill;
    }

    public void handleAddButtonOnAction(Button add, GridPane menu, TextField totalTf){
        add.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                SoldProduct soldProduct = new SoldProduct();
                try {
                    Product product = new ProductDBLayer().getProductByName(name);
                    if (name == null) {
                        throw new Exception("No product entered!");
                    }
                    if(product.getQuantity() < Integer.parseInt(quantityTf.getText())){
                        new AlertGUI().showAlertMessage("Error", "There are not enough products in stock!");
                    }
                    else{
                        setSoldProductValues(soldProduct, product);
                        int i = Integer.parseInt(quantityTf.getText());
                        soldProduct.setSoldQuantity(i);
                        addProductToMenu(soldProduct, menu);
                        soldProducts.add(soldProduct);
                        products.add(product);

                        double totalValue = Double.parseDouble(totalTf.getText()) + soldProduct.getSellingPrice() * soldProduct.getSoldQuantity();
                        totalTf.setText(totalValue + "");
                    }
                }
                catch(Exception e){
                    System.out.println(e.getMessage());
                }

            }
        });
    }

    private void addProductToMenu(SoldProduct soldProduct, GridPane menu) {

        Label idLabel = new Label(soldProduct.getId() + "");
        idLabel.setMinHeight(20);
        idLabel.setPadding(new Insets(0, 0, 0, 70));
        menu.add(idLabel, 0, row);

        Label nameLabel = new Label(soldProduct.getName());
        nameLabel.setMinHeight(20);
        nameLabel.setPadding(new Insets(0, 0, 0, 20));
        menu.add(nameLabel, 1, row);

        Label priceLabel = new Label(soldProduct.getSellingPrice() + "");
        priceLabel.setMinHeight(20);
        priceLabel.setPadding(new Insets(0, 0, 0, 36));
        menu.add(priceLabel, 2, row);


        Label quantityLabel = new Label(soldProduct.getSoldQuantity() + "");
        quantityLabel.setMinHeight(20);
        quantityLabel.setPadding(new Insets(0, 0, 0, 36));
        menu.add(quantityLabel, 3, row);
        row++;
    }

    private void setSoldProductValues(SoldProduct soldProduct, Product product) {
        soldProduct.setId(product.getId());
        soldProduct.setName(product.getName());
        soldProduct.setSellingPrice(product.getSellingPrice());
        soldProduct.setBillID(BillDBLayer.getCurrentBillID());


    }


    public static void handleLogOutButtonOnAction(Button logOutButton, Stage primaryStage){
        logOutButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                primaryStage.close();
                new LoginGUI().start();
            }
        });
    }

    public void handleNamesOnAction(){
        names.setOnAction((event) -> {

            name = names.getValue();
            try {
                Product product = new ProductDBLayer().getProductByName(name);
                id.setText(String.valueOf(product.getId()));
                quantityTf.setText("1");
            }
            catch (DBException e){
                System.out.println("Product not in db!");
            }
        });
    }

    public ComboBox<String> getNames() {
        return names;
    }

    public void setNames(ComboBox<String> names) {
        this.names = names;
    }
    public ArrayList<SoldProduct> getSoldProducts() {
        return soldProducts;
    }

    public void setSoldProducts(ArrayList<SoldProduct> soldProducts) {
        this.soldProducts = soldProducts;
    }

    public TextField getId() {
        return id;
    }

    public void setId(TextField id) {
        this.id = id;
    }

    public TextField getQuantityTf() {
        return quantityTf;
    }

    public void setQuantityTf(TextField quantityTf) {
        this.quantityTf = quantityTf;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

}
