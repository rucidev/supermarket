package com.gui.cashier;

import com.gui.SupermarketGUIAbstract;
import com.model.bill.Bill;
import com.dblayer.BillDBLayer;
import com.model.employee.User;
import com.model.product.Product;
import com.dblayer.ProductDBLayer;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.ArrayList;

public class CashierMenuGUI extends SupermarketGUIAbstract{
    private Stage primaryStage = null;
    private User user = null;
    private Bill bill = null;
    private CashierMenuHandler menuHandler = new CashierMenuHandler();
    GridPane menu = new GridPane();
    TextField totalTf = new TextField("0");

    public CashierMenuGUI(User user){
        this.user = user;
    }

    public void start(){
        primaryStage = new Stage();
        GridPane gridPane = new GridPane();

        addHeaderToMainPane(gridPane, primaryStage, user);
        addBillNoToMainPane(gridPane);
        addHeaderMenuToMainPane(gridPane);
        addMenuToMainPane(gridPane);
        addFooterToMainPane(gridPane);


        gridPane.setStyle("-fx-background-color: #fffae1");
        gridPane.setPadding(new Insets(20, 20, 20, 20));
        primaryStage.setTitle("Supermarket / Cashier");
        primaryStage.setScene(new Scene(gridPane, 1000, 700));
        primaryStage.show();
    }

    private void addFooterToMainPane(GridPane gridPane){
        Label totalLabel = new Label("TOTAL: ");
        totalLabel.setFont(new Font(30));
        totalTf.setPadding(new Insets(0, 200, 0, 0));
        totalTf.setAlignment(Pos.CENTER);
        totalTf.setMinHeight(50);
        totalTf.setEditable(false);
        Button okButton = new Button("OK");
        okButton.setMinHeight(50);
        okButton.setMinWidth(50);

        menuHandler.handleOkButtonOnAction(okButton, BillDBLayer.getCurrentBillID(), user, primaryStage);

        Button cancelButton = new Button("Cancel");
        cancelButton.setMinHeight(50);
        cancelButton.setMinWidth(70);
        menuHandler.handleCancelButton(cancelButton, user, primaryStage);

        HBox footer = new HBox(20);
        footer.setPadding(new Insets(50, 10, 10, 330));
        footer.getChildren().add(totalLabel);
        footer.getChildren().add(totalTf);
        footer.getChildren().add(okButton);
        footer.getChildren().add(cancelButton);

        gridPane.add(footer, 0, 4);
    }

    private void addMenuToMainPane(GridPane gridPane){
        menu.setGridLinesVisible(true);

        menu.setStyle("-fx-border-color: black");
        menu.setStyle("-fx-background-color: white");
        gridPane.add(menu, 0, 3);
        fillDefaultMenu();
    }

    private void fillDefaultMenu(){
        for(int i = 0 ; i< 13; i++){
            for(int j = 0; j < 4; j++){
                TextField tf = new TextField();
                tf.setEditable(false);
                tf.setMinHeight(30);
                tf.setStyle("-fx-border-color: black");
                if(j == 0){
                    tf.setMinWidth(160);
                }
                else if(j == 1){
                    tf.setMinWidth(535);
                }
                else{
                    tf.setMinWidth(100);
                }
                menu.add(tf, j, i);
            }
        }
    }

    private void addHeaderMenuToMainPane(GridPane gridPane){
        HBox hbox = new HBox(15);
        hbox.setPadding(new Insets(10, 0, 10, 0));
        TextField id = new TextField("ID");
        ComboBox<String> names = new ComboBox<>();
        TextField quantity = new TextField("quantity");
        Button addButton = new Button("Add");
        id.setEditable(false);
        names.setValue("Name");

        menuHandler.setNames(names);
        menuHandler.setId(id);
        menuHandler.setQuantityTf(quantity);
        menuHandler.handleNamesOnAction();
        menuHandler.handleAddButtonOnAction(addButton, menu, totalTf);
        id.setMinWidth(150);
        names.setMinWidth(300);
        addButton.setMinWidth(50);

        fillNames(names);

        hbox.getChildren().add(id);
        hbox.getChildren().add(names);
        hbox.getChildren().add(quantity);
        hbox.getChildren().add(addButton);

        new ComboBoxAutoComplete<String>(names);
        gridPane.add(hbox, 0, 2);
    }

    private void fillNames(ComboBox<String> names){
        ArrayList<Product> products = new ProductDBLayer().getProducts();
        for (Product product: products) {
            names.getItems().add(product.getName());
        }
    }

    private void addBillNoToMainPane(GridPane gridPane){
        int id = new BillDBLayer().getCurrentBillID();
        TextField billTextField = new TextField("Bill NO :  " + id);
        billTextField.setEditable(false);
        billTextField.setMaxWidth(150);
        billTextField.setAlignment(Pos.BASELINE_LEFT);
        gridPane.add(billTextField, 0 , 1);

    }




}
