package com.gui;

import javafx.scene.control.Alert;

public class AlertGUI {
    public static void showAlertMessage(String title, String text) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(text);
        alert.showAndWait();
    }
}
