package com.gui;

import com.gui.cashier.CashierMenuHandler;
import com.model.employee.User;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public abstract class SupermarketGUIAbstract {
    public abstract void start();


    public void addHeaderToMainPane(GridPane gridPane, Stage primaryStage, User user) {
        HBox hbox = new HBox(20);

        Label supermarketLabel = new Label("Supermarket");
        supermarketLabel.setFont(Font.font("Cascade", FontWeight.NORMAL, 33));
        supermarketLabel.setMinWidth(750);
        supermarketLabel.setMaxHeight(100);

        Text userText = new Text("User: " + user.getName());
        userText.setFont(Font.font("Arial", FontWeight.NORMAL, 18));

        Button logoutButton = new Button("Logout");
        CashierMenuHandler.handleLogOutButtonOnAction(logoutButton, primaryStage);

        hbox.getChildren().addAll(supermarketLabel, userText, logoutButton);
        hbox.setAlignment(Pos.CENTER);

        gridPane.add(hbox, 0, 0);
    }
}
