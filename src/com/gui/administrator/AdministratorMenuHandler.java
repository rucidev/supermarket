package com.gui.administrator;

import com.commons.date.ASUtilDate;
import com.dblayer.ProductDBLayer;
import com.dblayer.SoldProductDBLayer;
import com.dblayer.UserDBLayer;
import com.gui.AlertGUI;
import com.gui.cashier.CashierMenuGUI;
import com.gui.economist.EconomistMenuGUI;
import com.gui.economist.EconomistMenuHandler;
import com.model.employee.Employee;
import com.model.employee.User;
import com.model.product.Product;
import com.model.product.SoldProduct;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.util.ArrayList;

public class AdministratorMenuHandler {

    public static void handleCalculateButtonOnAction(Button cancelButton, TextField date1, TextField date2, Label label1, Label label2){
        cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ArrayList<SoldProduct> sold = new SoldProductDBLayer().getSoldProductsByDate(date1.getText(), date2.getText());
                ArrayList<Product> products = new ProductDBLayer().getProductsByDate(date1.getText(), date2.getText());
                double income = 0;
                double cost = 0;
                for(int i = 0 ; i < sold.size(); i++){
                    income += sold.get(i).getSellingPrice();
                }
                for(int i = 0 ; i < products.size(); i++){
                    cost += products.get(i).getBoughtPrice();
                }
                label1.setText("Total Income: " + income);
                label2.setText("Total Cost: " + cost);
            }
        });
    }

    public static void handleRegisterButtonOnAction(Button registerButton, TextField nameTf, TextField birthdayTf, TextField phoneTf, TextField emailTf, TextField salaryTf, TextField accessLevelTf) {
        registerButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    User user = new UserDBLayer().getUserByName(nameTf.getText());
                    if (user == null) {
                        user = new User();
                        user.setName(nameTf.getText());
                        user.setBirthday(new ASUtilDate(birthdayTf.getText()));
                        user.setMobileNo(phoneTf.getText());
                        user.setEmail(emailTf.getText());
                        user.setSalary(Double.parseDouble(salaryTf.getText()));
                        checkAccessLevel(user, accessLevelTf);
                        user.setUsername(nameTf.getText());
                        user.setPassword("admin");
                        new UserDBLayer().saveUser(user);
                        AlertGUI.showAlertMessage("Success", "Account was created with password 'admin' !");
                    } else {
                        AlertGUI.showAlertMessage("Error", "Account already exists!");
                    }
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }
            }
        });
    }

    private static void checkAccessLevel(User user, TextField accessLevelTf) {
        if (accessLevelTf.getText().equals("ADMINISTRATOR")) {
            user.setAccessLevel(2);
        } else if (accessLevelTf.getText().equals("ECONOMIST")) {
            user.setAccessLevel(1);
        } else {
            user.setAccessLevel(0);
        }
    }

    public static void handleModifyButtonOnAction(Button modifyButton, TextField nameTf, TextField birthdayTf, TextField phoneTf, TextField emailTf, TextField salaryTf, TextField accessLevelTf) {
        modifyButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    User user = new UserDBLayer().getUserByName(nameTf.getText());
                    if (user == null) {
                        AlertGUI.showAlertMessage("Error", "Name of the user cannot be modified!");
                    } else {
                        user = new User();
                        user.setName(nameTf.getText());
                        user.setBirthday(new ASUtilDate(birthdayTf.getText()));
                        user.setMobileNo(phoneTf.getText());
                        user.setEmail(emailTf.getText());
                        user.setSalary(Double.parseDouble(salaryTf.getText()));
                        checkAccessLevel(user, accessLevelTf);
                        user.setUsername(nameTf.getText());
                        user.setPassword("admin");
                        new UserDBLayer().updateUser(user);
                        AlertGUI.showAlertMessage("Success", "Account modified !");
                    }
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }
            }
        });
    }

    public static void handleDeleteButtonOnAction(Button modifyButton, TextField nameTf, TextField birthdayTf, TextField phoneTf, TextField emailTf, TextField salaryTf, TextField accessLevelTf, User user, Stage stage) {
        modifyButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    User user = new UserDBLayer().getUserByName(nameTf.getText());

                    user = new User();
                    user.setName(nameTf.getText());
                    new UserDBLayer().deleteUser(user);
                    AlertGUI.showAlertMessage("Success", "Account deleted !");
                    new AdministratorOperationsGUI(user).start();
                    stage.close();
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }
            }
        });
    }

    public static void handleEmployeesComboBxOnAction(ComboBox<String> employeesComboBox, TextField nameTf, TextField birthdayTf, TextField phoneTf, TextField emailTf, TextField salaryTf, TextField accessLevelTf) {
        employeesComboBox.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    String employeeName = employeesComboBox.getValue();
                    User user = new UserDBLayer().getUserByName(employeeName);
                    nameTf.setText(user.getName());
                    birthdayTf.setText(user.getBirthday().toFileDate());
                    phoneTf.setText(user.getMobileNo());
                    emailTf.setText(user.getEmail());
                    salaryTf.setText(user.getSalary() + "");
                    if (user.getAccessLevel() == Employee.ADMINISTRATOR) {
                        accessLevelTf.setText("ADMINISTRATOR");
                    }
                    if (user.getAccessLevel() == Employee.CASHIER) {
                        accessLevelTf.setText("CASHIER");
                    }
                    if (user.getAccessLevel() == Employee.ECONOMIST) {
                        accessLevelTf.setText("ECONOMIST");
                    }

                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }
            }
        });
    }

    public static void handleAdministratorButtonOnAction(Button administratorButton, Stage stage, User user) {
        administratorButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                stage.close();
                new AdministratorOperationsGUI(user).start();
            }
        });
    }

    public static void handleCashierButtonOnAction(Button cashierButton, Stage stage, User user) {
        cashierButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                stage.close();
                new CashierMenuGUI(user).start();
            }
        });
    }

    public static void handleEconomistButtonOnAction(Button economistButton, Stage stage, User user) {
        economistButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                stage.close();
                new EconomistMenuGUI(user).start();
            }
        });
    }
}
