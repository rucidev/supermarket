package com.gui.administrator;

import com.gui.SupermarketGUIAbstract;
import com.model.employee.User;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class AdministratorMenuGUI extends SupermarketGUIAbstract{
    private User user = null;
    private Stage stage = null;

    public AdministratorMenuGUI(User user){
        this.user = user;
    }

    public void start(){
        stage = new Stage();
        GridPane pane = new GridPane();
        pane.setAlignment(Pos.CENTER);
        pane.setHgap(10);
        pane.setVgap(10);
        pane.setPadding(new Insets(25, 25, 25, 25));
        pane.setAlignment(Pos.CENTER);

        pane.add(getUserMenu(), 0, 0);

        stage.setTitle("Administrator");
        stage.setScene(new Scene(pane, 300, 275));
        stage.show();
    }

    public VBox getUserMenu(){
        VBox vBox = new VBox(20);

        Button cashierButton = new Button("Cashier");
        Button economistButton = new Button("Economist");
        Button administratorButton = new Button("Administrator");
        cashierButton.setMinSize(200, 50);
        economistButton.setMinSize(200, 50);
        administratorButton.setMinSize(200, 50);

        AdministratorMenuHandler.handleAdministratorButtonOnAction(administratorButton, stage, user);
        AdministratorMenuHandler.handleCashierButtonOnAction(cashierButton, stage, user);
        AdministratorMenuHandler.handleEconomistButtonOnAction(economistButton, stage, user);

        vBox.getChildren().addAll(cashierButton, economistButton, administratorButton);

        return vBox;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
