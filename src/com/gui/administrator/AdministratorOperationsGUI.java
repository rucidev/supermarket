package com.gui.administrator;

import com.commons.date.ASUtilDate;
import com.dblayer.UserDBLayer;
import com.gui.SupermarketGUIAbstract;
import com.gui.cashier.CashierMenuHandler;
import com.model.employee.User;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.ArrayList;

public class AdministratorOperationsGUI extends SupermarketGUIAbstract{
    private User user = null;
    private Stage stage = null;
    private TextField name = null;
    private TextField birthday = null;
    private TextField phone = null;
    private TextField email = null;
    private TextField salary = null;
    private TextField accessLevel = null;
    private ComboBox<String> usersComboBox = null;
    private Button bRegister = null;
    private Button bModify = null;
    private Button bDelete = null;
    private Button calculateButton = null;
    private Label totalIncomeLabel = null;
    private Label totalCostLabel = null;
    private TextField firstDateTf = null;
    private TextField secondDateTf = null;


    public AdministratorOperationsGUI(User user) {
        this.user = user;
    }

    public void start() {
        stage = new Stage();
        GridPane pane = new GridPane();
        pane.setStyle("-fx-background-color: #fffae1");
        pane.add(getHeaderHBox(), 0, 0);

        HBox hBox = new HBox(50);
        hBox.setPadding(new Insets(50, 0, 0, 10));

        hBox.getChildren().add(getOperationButtonsVBox());
        hBox.getChildren().add(getUserInformationGridPane());
        hBox.getChildren().add(getStatsVBox());

        AdministratorMenuHandler.handleEmployeesComboBxOnAction(usersComboBox, name, birthday, phone, email, salary, accessLevel);
        AdministratorMenuHandler.handleRegisterButtonOnAction(bRegister, name, birthday, phone, email, salary, accessLevel);
        AdministratorMenuHandler.handleModifyButtonOnAction(bModify, name, birthday, phone, email, salary, accessLevel);
        AdministratorMenuHandler.handleDeleteButtonOnAction(bDelete, name, birthday, phone, email, salary, accessLevel, user, stage);
        AdministratorMenuHandler.handleCalculateButtonOnAction(calculateButton, firstDateTf, secondDateTf, totalIncomeLabel, totalCostLabel);

        pane.add(hBox, 0, 1);

        stage.setTitle("Administrator");
        stage.setScene(new Scene(pane, 1000, 500));
        stage.show();
    }

    private HBox getHeaderHBox() {
        HBox hbox = new HBox(20);
        hbox.setPadding(new Insets(30, 0, 0, 0));

        Label supermarketLabel = new Label("Supermarket");
        supermarketLabel.setFont(Font.font("Cascade", FontWeight.NORMAL, 33));
        supermarketLabel.setMinWidth(750);
        supermarketLabel.setMaxHeight(100);

        Text userText = new Text("User: " + user.getName());
        userText.setFont(Font.font("Arial", FontWeight.NORMAL, 18));

        Button logoutButton = new Button("Logout");
        CashierMenuHandler.handleLogOutButtonOnAction(logoutButton, stage);

        hbox.getChildren().addAll(supermarketLabel, userText, logoutButton);
        hbox.setAlignment(Pos.CENTER);

        return hbox;
    }

    public VBox getStatsVBox() {
        VBox vbox = new VBox(5);

        HBox hBox = new HBox(5);
        firstDateTf = new TextField(new ASUtilDate().toFileDate());
        secondDateTf = new TextField(new ASUtilDate().toFileDate());
        calculateButton = new Button("Calculate");
        calculateButton.setMinWidth(303);

        firstDateTf.setMinWidth(50);
        secondDateTf.setMinWidth(50);

        hBox.getChildren().addAll(firstDateTf, secondDateTf);

        totalIncomeLabel = new Label("Total income: ");
        totalCostLabel = new Label("Total cost: ");
        totalIncomeLabel.setPadding(new Insets(10, 0, 0, 0));
        totalIncomeLabel.setFont(new Font(20));
        totalCostLabel.setFont(new Font(20));
        totalIncomeLabel.setMinWidth(105);
        totalCostLabel.setMinWidth(105);

        vbox.getChildren().addAll(hBox, calculateButton, totalIncomeLabel, totalCostLabel);

        return vbox;
    }


    public GridPane getUserInformationGridPane() {
        GridPane pane = new GridPane();

        TextField nameTf = new TextField("Name:");
        nameTf.setEditable(false);
        nameTf.setMinWidth(180);
        nameTf.setMinHeight(40);

        TextField birthdayTf = new TextField("Birthday:");
        birthdayTf.setMinWidth(180);
        birthdayTf.setMinHeight(40);
        birthdayTf.setEditable(false);

        TextField phoneTf = new TextField("Phone:");
        phoneTf.setMinWidth(180);
        phoneTf.setMinHeight(40);
        phoneTf.setEditable(false);

        TextField emailTf = new TextField("Email:");
        emailTf.setMinWidth(180);
        emailTf.setMinHeight(40);
        emailTf.setEditable(false);

        TextField salaryTf = new TextField("Salary:");
        salaryTf.setMinWidth(180);
        salaryTf.setMinHeight(40);
        salaryTf.setEditable(false);

        TextField accessLevelTf = new TextField("Access Level:");
        accessLevelTf.setMinWidth(180);
        accessLevelTf.setMinHeight(40);
        accessLevelTf.setEditable(false);

        name = new TextField();
        name.setMinWidth(180);
        name.setMinHeight(40);

        birthday = new TextField();
        birthday.setMinWidth(180);
        birthday.setMinHeight(40);

        phone = new TextField();
        phone.setMinWidth(180);
        phone.setMinHeight(40);

        email = new TextField();
        email.setMinWidth(180);
        email.setMinHeight(40);

        salary = new TextField();
        salary.setMinWidth(180);
        salary.setMinHeight(40);

        accessLevel = new TextField();
        accessLevel.setMinWidth(180);
        accessLevel.setMinHeight(40);


        pane.add(nameTf, 0, 0);
        pane.add(name, 1, 0);
        pane.add(birthdayTf, 0, 1);
        pane.add(birthday, 1, 1);
        pane.add(phoneTf, 0, 2);
        pane.add(phone, 1, 2);
        pane.add(emailTf, 0, 3);
        pane.add(email, 1, 3);
        pane.add(salaryTf, 0, 4);
        pane.add(salary, 1, 4);
        pane.add(accessLevelTf, 0, 5);
        pane.add(accessLevel, 1, 5);


        return pane;
    }


    public VBox getOperationButtonsVBox() {
        VBox vBox = new VBox(10);
        vBox.setPadding(new Insets(0, 0, 0, 20));
        usersComboBox = new ComboBox<>();
        bRegister = new Button("Register");
        bRegister.setMinWidth(180);

        bModify = new Button("Modify");
        bModify.setMinWidth(180);
        bDelete = new Button("Delete");
        bDelete.setMinWidth(180);

        usersComboBox.setMinWidth(180);
        usersComboBox.setValue("Employees");
        ArrayList<User> users = new ArrayList<>();
        users = new UserDBLayer().getUsers();
        for (User user : users) {
            usersComboBox.getItems().add(user.getName());
        }
        StackPane stackPane = new StackPane();
        stackPane.getChildren().add(usersComboBox);
        stackPane.setPadding(new Insets(0, 0, 110, 0));

        vBox.getChildren().addAll(stackPane, bRegister, bModify, bDelete);
        return vBox;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
