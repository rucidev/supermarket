package com.stats;

import com.commons.entity.Entity;

import java.util.ArrayList;

public class CashierStats {
    private int EmployeeID = Entity.MINUS_ONE;
    private ArrayList<DailyBillStats> billStats= null;

    public ArrayList<DailyBillStats> getBillStats() {
        if(billStats == null){
            billStats = new ArrayList<>();
        }
        return billStats;
    }

    public void setBillStats(ArrayList<DailyBillStats> billStats) {
        this.billStats = billStats;
    }

    public int getEmployeeID() {
        return EmployeeID;
    }

    public void setEmployeeID(int employeeID) {
        EmployeeID = employeeID;
    }
}
