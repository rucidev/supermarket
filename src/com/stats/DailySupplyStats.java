package com.stats;

import com.commons.date.ASUtilDate;
import com.model.product.BoughtProduct;

import java.util.ArrayList;

public class DailySupplyStats {
    private ASUtilDate date = null;
    private ArrayList<BoughtProduct> products = null;

    public ArrayList<BoughtProduct> getProducts() {
        if(products == null){
            products = new ArrayList<>();
        }
        return products;
    }

    public void setProducts(ArrayList<BoughtProduct> products) {
        this.products = products;
    }

    public ASUtilDate getDate() {
        return date;
    }

    public void setDate(ASUtilDate date) {
        this.date = date;
    }
}
