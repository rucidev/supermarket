package com.stats;

import com.commons.date.ASUtilDate;
import com.model.bill.Bill;

import java.util.ArrayList;

public class DailyBillStats {
    private ASUtilDate date = null;
    private ArrayList<Bill> bills = null;

    public ASUtilDate getDate() {
        return date;
    }

    public void setDate(ASUtilDate date) {
        this.date = date;
    }

    public ArrayList<Bill> getBills() {
        return bills;
    }

    public void setBills(ArrayList<Bill> bills) {
        this.bills = bills;
    }
}
