package com.stats;

import com.commons.date.ASUtilDate;
import com.model.employee.Employee;

import java.util.ArrayList;

public class DailySalariesStats {
    private ASUtilDate date = null;
    private ArrayList<Employee> employees = null;

    public ASUtilDate getDate() {
        return date;
    }

    public void setDate(ASUtilDate date) {
        this.date = date;
    }

    public ArrayList<Employee> getEmployees() {
        if(employees == null){
            employees = new ArrayList<>();
        }
        return employees;
    }

    public void setEmployees(ArrayList<Employee> employees) {
        this.employees = employees;
    }
}
